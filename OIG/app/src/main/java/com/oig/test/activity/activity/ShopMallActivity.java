package com.oig.test.activity.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.NonSwipeableViewPager;
import com.oig.test.activity.adapter.AdPagerAdpater;
import com.oig.test.activity.adapter.ShopMallCatAdpater;
import com.oig.test.activity.model.AdvertisementModel;
import com.oig.test.activity.model.CategoryModel;
import com.oig.test.activity.model.ShopCategoryModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ShopMallActivity extends BaseActivity {

    int currentPage = 0;
    //private static int IMAGE_TIME_OUT = 6500;
    private static final String TAG = "ShopMallActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView malllist;
    ShopMallCatAdpater shopMallCatAdpater;

    ShopCategoryModel shopCategoryModel = null;
    List<CategoryModel> shopCategoryModels = new ArrayList<>();
    List<AdvertisementModel> advertisementModels = new ArrayList<>();

    Subscription subscription;

    @BindView(R.id.img_closeads)
    ImageView imgCloseads;
    @BindView(R.id.rel_flliper)
    RelativeLayout relFlliper;
    @BindView(R.id.li_notfound)
    LinearLayout liNotfound;
    @BindView(R.id.pager_ads)
    NonSwipeableViewPager pagerAds;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_mall);
        ButterKnife.bind(this);

        tvTittle.setText(R.string.shop_mall);
        Log.d(TAG, "onCreate: >>Shopmall");


        if (L.isNetworkAvailable(ShopMallActivity.this)) {
            getMallCategory();
        }

        malllist.setHasFixedSize(true);
        malllist.setItemAnimator(new DefaultItemAnimator());
        malllist.setLayoutManager(new LinearLayoutManager(this));
        shopMallCatAdpater = new ShopMallCatAdpater(this, shopCategoryModels);
        malllist.setAdapter(shopMallCatAdpater);
    }

    @OnClick({R.id.img_back, R.id.tv_tittle, R.id.img_closeads, R.id.rel_flliper})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_closeads:
                relFlliper.setVisibility(View.GONE);
                break;
            case R.id.rel_flliper:
                if (advertisementModels != null && advertisementModels.size() > 0) {
                    String url = advertisementModels.get(0).getAdvtwebURL().contains("http") ? advertisementModels.get(0).getAdvtwebURL() : "http://" + advertisementModels.get(currentPage).getAdvtwebURL();
                }
                break;

        }
    }

    private void getMallCategory() {
        Map<String, String> map = new HashMap<>();

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getShopMallCat(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.data)) {


                        shopCategoryModel = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), ShopCategoryModel.class);
                        advertisementModels.clear();
                        advertisementModels.addAll(shopCategoryModel.getAdvt());
                        shopCategoryModels.clear();
                        shopCategoryModels.addAll(shopCategoryModel.getShopCategories());
                        shopMallCatAdpater.notifyDataSetChanged();

                        if (advertisementModels.size() > 0) {
                            relFlliper.setVisibility(View.VISIBLE);

                            pagerAds.setAdapter(new AdPagerAdpater(ShopMallActivity.this, advertisementModels));

                           /* new Handler().postDelayed(() -> {
                                relFlliper.setVisibility(View.GONE);
                            }, IMAGE_TIME_OUT);*/
                        } else {
                            relFlliper.setVisibility(View.GONE);
                        }

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
