package com.oig.test.activity.adapter;

import android.app.Dialog;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.github.chrisbanes.photoview.PhotoView;
import com.oig.test.R;
import com.oig.test.activity.model.ProductsImage;
import com.oig.test.activity.util.L;

import java.util.List;

public class PagerSliderAdapter extends PagerAdapter {


    private List<ProductsImage> productsImages;
    private LayoutInflater inflater;
    private Context context;


    public PagerSliderAdapter(Context context, List<ProductsImage> productsImages) {
        this.context = context;
        this.productsImages = productsImages;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return productsImages.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.image_slider_pager, view, false);

        assert imageLayout != null;
        ProductsImage productsImage = productsImages.get(position);

        final ImageView imageView = imageLayout.findViewById(R.id.img_pager);

        L.loadImageWithPicasso(context, productsImage.getImageURL(), imageView, null);

        view.addView(imageLayout, 0);

        imageView.setOnClickListener(view1 -> {

            Dialog dialog;
            dialog = new Dialog(context, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.imagezoomview);
            PhotoView photoView = dialog.findViewById(R.id.img_zoom);
            L.loadImageWithPicasso(context, productsImage.getImageURL(), photoView, null);
            dialog.show();

        });
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}