package com.oig.test.activity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserPrefActivity extends BaseActivity {

    @BindView(R.id.tv_seller)
    TextView tvseller;
    @BindView(R.id.tv_buyer)
    TextView tvBuyer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_pref);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.tv_seller, R.id.tv_buyer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_buyer:
                Intent intent = new Intent(UserPrefActivity.this, Dashboard.class);
                intent.putExtra(Constant.selectTab, 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.tv_seller:
                Intent intent1 = new Intent(UserPrefActivity.this, Dashboard.class);
                intent1.putExtra(Constant.selectTab, 1);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                break;
        }
    }
}
