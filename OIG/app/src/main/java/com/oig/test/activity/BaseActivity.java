package com.oig.test.activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.google.gson.Gson;
import com.oig.test.R;
import com.oig.test.activity.model.UserData;
import com.oig.test.activity.network.RestAPIBuilder;
import com.oig.test.activity.network.RestApi;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.Prefs;

public class BaseActivity extends AppCompatActivity {

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected Gson gson;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        restApi = RestAPIBuilder.buildRetrofitService();
        prefs = Prefs.with(this);
        gson = new Gson();
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
    }

    public void showProgress(boolean isShow) {
        if (dialog == null) {
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_show);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        if (isShow) {
            dialog.show();
        } else {
            dialog.dismiss();
        }
    }
}
