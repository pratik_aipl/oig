package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class FiledModel implements Serializable {

    @JsonField
    String NofFields;
    @JsonField
    String Label;
    @JsonField
    String Fields;

    public String getNofFields() {
        return NofFields;
    }

    public void setNofFields(String nofFields) {
        NofFields = nofFields;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String label) {
        Label = label;
    }

    public String getFields() {
        return Fields;
    }

    public void setFields(String fields) {
        Fields = fields;
    }
}
