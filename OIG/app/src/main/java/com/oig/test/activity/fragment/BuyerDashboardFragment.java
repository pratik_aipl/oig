package com.oig.test.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.R;
import com.oig.test.activity.activity.ShopMallActivity;
import com.oig.test.activity.adapter.Dashboardadpater;
import com.oig.test.activity.model.CategoryModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Subscription;


public class BuyerDashboardFragment extends BaseFragment {

    @BindView(R.id.recyclerlist)
    RecyclerView recyclerlist;
    Dashboardadpater dashboardadpater;
    @BindView(R.id.li_ShopMall)
    LinearLayout liShopMall;
    private Unbinder unbinder;

    Subscription subscription;
    List<CategoryModel> categoryModels = new ArrayList<>();

    public BuyerDashboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        dashboardadpater = new Dashboardadpater(getActivity(), categoryModels);
        recyclerlist.setAdapter(dashboardadpater);

        if (L.isNetworkAvailable(getActivity()))
            getcategory();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.li_ShopMall})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.li_ShopMall:
                startActivity(new Intent(getActivity(),
                        ShopMallActivity.class)
                        .putExtra(Constant.IsFrom, "Buyer"));
                break;
        }
    }

    private void getcategory() {
        Map<String, String> map = new HashMap<>();

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getcategory(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    categoryModels.clear();
                    categoryModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.Categories).toString(), CategoryModel.class));
                    dashboardadpater.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

}
