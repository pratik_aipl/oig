package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class ShopCategoryModel implements Serializable {

    @JsonField
    List<AdvertisementModel> Advt;
    @JsonField
    List<CategoryModel> ShopCategories;

    public List<AdvertisementModel> getAdvt() {
        return Advt;
    }

    public void setAdvt(List<AdvertisementModel> advt) {
        Advt = advt;
    }

    public List<CategoryModel> getShopCategories() {
        return ShopCategories;
    }

    public void setShopCategories(List<CategoryModel> shopCategories) {
        ShopCategories = shopCategories;
    }
}
