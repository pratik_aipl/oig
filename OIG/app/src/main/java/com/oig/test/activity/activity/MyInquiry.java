package com.oig.test.activity.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.adapter.MyInquiryAdpater;
import com.oig.test.activity.model.MyInquiryModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class MyInquiry extends BaseActivity {

    MyInquiryAdpater myInquiryAdpater;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerList;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_back)
    ImageView imgBack;

    Subscription subscription;
    List<MyInquiryModel> myInquiries = new ArrayList<>();
    @BindView(R.id.li_notfound)
    LinearLayout liNotfound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_inquiry);
        ButterKnife.bind(this);

        tvTittle.setText("My Inquiry");
        if (L.isNetworkAvailable(this))
            getInquiry();

        recyclerList.setHasFixedSize(true);
        recyclerList.setItemAnimator(new DefaultItemAnimator());
        recyclerList.setLayoutManager(new LinearLayoutManager(this));

        myInquiryAdpater = new MyInquiryAdpater(this, myInquiries);
        recyclerList.setAdapter(myInquiryAdpater);
    }

    @OnClick({R.id.img_back, R.id.tv_tittle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;

        }
    }

    private void getInquiry() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getMyInquiry(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data))
                        if (jsonResponse.getJSONObject(Constant.data).has(Constant.MyInquiry)) {
                            myInquiries.clear();
                            myInquiries.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.MyInquiry).toString(), MyInquiryModel.class));
                            myInquiryAdpater.notifyDataSetChanged();
                        } else {
                            recyclerList.setVisibility(View.GONE);
                            liNotfound.setVisibility(View.VISIBLE);
                        }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
