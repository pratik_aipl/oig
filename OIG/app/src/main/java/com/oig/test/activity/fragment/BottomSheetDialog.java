package com.oig.test.activity.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.oig.test.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheetDialog extends BottomSheetDialogFragment {

    @BindView(R.id.img_cancel)
    ImageView imgCancel;
    Unbinder unbinder;
    @BindView(R.id.btn_apply)
    Button btnApply;
    @BindView(R.id.rd_low)
    RadioButton rdLow;
    @BindView(R.id.rd_high)
    RadioButton rdHigh;
    @BindView(R.id.rdg_price)
    RadioGroup rdgPrice;


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.bottomsheet_filter, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }


    @OnClick({R.id.img_cancel, R.id.btn_apply})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_cancel:
                dismiss();
                break;
            case R.id.btn_apply:
                break;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
