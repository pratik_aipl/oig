package com.oig.test.activity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.model.UserData;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.network.RestApi;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;
import com.oig.test.activity.util.Validation;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

public class VerificationActivity extends BaseActivity {

    private static final String TAG = "VerificationActivity";
    @BindView(R.id.edt_verifycode)
    EditText edtVerifycode;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    @BindView(R.id.tv_resend)
    TextView tvResend;

    String otp, mobileno, isfrom;
    Subscription subscription;
    public String registrationId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);


        otp = getIntent().getStringExtra(Constant.Otp);
        mobileno = getIntent().getStringExtra(Constant.MOBILENO);

        isfrom = getIntent().getStringExtra(Constant.IsFrom);

        if (BuildConfig.DEBUG)
            edtVerifycode.setText("123456");

        OneSignal.idsAvailable((userId, registrationId) -> {
            Log.d(TAG, "onCreate: User:" + userId);
            this.registrationId = userId;
        });

    }

    @OnClick({R.id.btn_verify, R.id.tv_resend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_verify:
                if (Validation.isEmpty(L.getEditText(edtVerifycode))) {
                    edtVerifycode.setError("Please enter Verification Code");
                } else {
                    if (L.isNetworkAvailable(VerificationActivity.this))
                        if (isfrom.equalsIgnoreCase("Login")) {
                            callverify();
                        } else {
                            if (!L.getEditText(edtVerifycode).equalsIgnoreCase(otp)) {
                                Toast.makeText(this, "Enter Valid OTP", Toast.LENGTH_SHORT).show();
                            } else {
                                Register();
                            }
                        }
                }
                break;
            case R.id.tv_resend:

                if (L.isNetworkAvailable(VerificationActivity.this)) {

                    callresendotp();
                }

                break;
        }
    }

    public void Register() {
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Constant.DeviceID, RestApi.createRequestBody(L.getDeviceId(this)));
        map.put(Constant.USERName, RestApi.createRequestBody(getIntent().getStringExtra(Constant.USERName)));
        map.put(Constant.SURNAME, RestApi.createRequestBody(getIntent().getStringExtra(Constant.SURNAME)));
        map.put(Constant.MOBILE, RestApi.createRequestBody(getIntent().getStringExtra(Constant.MOBILE)));
        map.put(Constant.EMAIL, RestApi.createRequestBody(getIntent().getStringExtra(Constant.EMAIL)));
        map.put(Constant.GENDER, RestApi.createRequestBody(getIntent().getStringExtra(Constant.GENDER)));
        map.put(Constant.DOB, RestApi.createRequestBody(getIntent().getStringExtra(Constant.DOB)));
        map.put(Constant.LOCATION_ID, RestApi.createRequestBody(getIntent().getStringExtra(Constant.LOCATION_ID)));
        // map.put(Constant.DASHBOARD_PREF, RestApi.createRequestBody(getIntent().getStringExtra(Constant.DASHBOARD_PREF)));
        MultipartBody.Part body = null;

        if (!TextUtils.isEmpty(getIntent().getStringExtra(Constant.Img_Path))) {
            File imgFile = new File(Objects.requireNonNull(getIntent().getStringExtra(Constant.Img_Path)));
            if (imgFile != null && !TextUtils.isEmpty(imgFile.getAbsolutePath())) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile);
                body = MultipartBody.Part.createFormData(Constant.ProfileImage, imgFile.getName(), requestFile);
            }
        }
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getRegister(map, body), (data) -> {
            showProgress(false);
            if (data.code() == 200) {

                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    prefs.save(Constant.loginAuthToken, DATA.getString("login_token"));
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.isLogin, true);

                    Intent intent = new Intent(this, UserPrefActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    private void callverify() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, mobileno);
        map.put(Constant.Otp, L.getEditText(edtVerifycode));
        map.put(Constant.DeviceID, L.getDeviceId(this));
        map.put(Constant.PlayerID, registrationId);


        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getverification(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    prefs.save(Constant.loginAuthToken, DATA.getString("login_token"));
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.isLogin, true);

                    Intent intent = new Intent(this, UserPrefActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void callresendotp() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, mobileno);
        map.put(Constant.DeviceID, L.getDeviceId(this));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getresendotp(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }
}
