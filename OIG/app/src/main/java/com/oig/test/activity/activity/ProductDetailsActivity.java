package com.oig.test.activity.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.adapter.PagerSliderAdapter;
import com.oig.test.activity.adapter.ProductDetailAdapter;
import com.oig.test.activity.model.DetailModel;
import com.oig.test.activity.model.ProductsList;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ProductDetailsActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.pager_prod)
    ViewPager pagerProd;
    @BindView(R.id.img_left)
    ImageView imgLeft;
    @BindView(R.id.img_right)
    ImageView imgRight;
    @BindView(R.id._tv_count)
    TextView TvCount;
    @BindView(R.id.tv_proname)
    TextView tvProname;

    int qsize;
    ProductsList productsList;
    @BindView(R.id.tv_sku)
    TextView tvSku;
    @BindView(R.id.tv_rate)
    TextView tvRate;
    @BindView(R.id.img_call)
    ImageView imgCall;
    @BindView(R.id.img_share)
    ImageView imgShare;
    @BindView(R.id.tv_desc)
    TextView tvDesc;
    @BindView(R.id.mDataView)
    LinearLayout mDataView;
    @BindView(R.id.mDetailsView)
    RecyclerView mDetailsView;
    String Isfrom;
    int ProID;

    List<DetailModel> detailModels = new ArrayList<>();
    ProductDetailAdapter productDetailAdapter;
    Subscription subscription;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    String phone;
    @BindView(R.id.img_dlt)
    ImageView imgDlt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        tvTittle.setText("Product Detail");

        ProID = getIntent().getIntExtra(Constant.ProID, 0);
        productsList = (ProductsList) getIntent().getSerializableExtra(Constant.Products);
        Isfrom = getIntent().getStringExtra(Constant.IsFrom);

        if (Isfrom.equalsIgnoreCase("MyProduct")) {
            imgEdit.setVisibility(View.VISIBLE);
            imgDlt.setVisibility(View.VISIBLE);
            imgCall.setVisibility(View.GONE);
        } else if (Isfrom.equalsIgnoreCase("NEW")) {
            imgCall.setVisibility(View.GONE);
            //imgShare.setVisibility(View.GONE);
            imgEdit.setVisibility(View.GONE);
            imgDlt.setVisibility(View.GONE);
        }
        detailModels.clear();
        if (!TextUtils.isEmpty(productsList.getFieldValue1())) {
            detailModels.add(new DetailModel(productsList.getLabel1(), productsList.getFieldValue1()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue2())) {
            detailModels.add(new DetailModel(productsList.getLabel2(), productsList.getFieldValue2()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue3())) {
            detailModels.add(new DetailModel(productsList.getLabel3(), productsList.getFieldValue3()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue4())) {
            detailModels.add(new DetailModel(productsList.getLabel4(), productsList.getFieldValue4()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue5())) {
            detailModels.add(new DetailModel(productsList.getLabel5(), productsList.getFieldValue5()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue6())) {
            detailModels.add(new DetailModel(productsList.getLabel6(), productsList.getFieldValue6()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue7())) {
            detailModels.add(new DetailModel(productsList.getLabel7(), productsList.getFieldValue7()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue8())) {
            detailModels.add(new DetailModel(productsList.getLabel8(), productsList.getFieldValue8()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue9())) {
            detailModels.add(new DetailModel(productsList.getLabel9(), productsList.getFieldValue9()));
        }
        if (!TextUtils.isEmpty(productsList.getFieldValue10())) {
            detailModels.add(new DetailModel(productsList.getLabel10(), productsList.getFieldValue10()));
        }
        if (detailModels.size() != 0) {
            mDataView.setVisibility(View.VISIBLE);
        } else {
            mDataView.setVisibility(View.GONE);
        }
        productDetailAdapter = new ProductDetailAdapter(this, detailModels);
        mDetailsView.setAdapter(productDetailAdapter);
        init();


        pagerProd.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == (pagerProd.getAdapter().getCount() - 1)) {
                    imgRight.setImageResource(R.drawable.unselectright);

                } else {
                    TvCount.setText((pagerProd.getCurrentItem() - 1) + "/ " + qsize);
                    imgRight.setImageResource(R.drawable.slider_right_btn);
                }

                if (position == 0) {
                    imgLeft.setImageResource(R.drawable.unselectleft);
                    TvCount.setText("1" + "/ " + productsList.getProductImages().size());

                } else {
                    TvCount.setText((pagerProd.getCurrentItem() + 1) + "/ " + qsize);
                    imgLeft.setImageResource(R.drawable.slider_left_btn);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @OnClick({R.id.img_back, R.id.img_left, R.id.img_right, R.id.img_call, R.id.img_share, R.id.img_edit, R.id.img_dlt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_edit:
                startActivity(new Intent(ProductDetailsActivity.this, EditProductDetails.class)
                        .putExtra(Constant.Products, productsList));
                break;

            case R.id.img_dlt:
                deleteProduct();
                break;
            case R.id.img_left:
                pagerProd.setCurrentItem(pagerProd.getCurrentItem() - 1, true);
                break;
            case R.id.img_right:
                pagerProd.setCurrentItem(pagerProd.getCurrentItem() + 1, true);
                break;
            case R.id.img_call:
                addProductInquiry();
                if (productsList.getIsPaid().equalsIgnoreCase("0")) {
                    phone = productsList.getAdmin_number();
                } else {
                    phone = productsList.getMobile();
                }
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
            case R.id.img_share:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "OIG App");
                    String shareMessage = "Buy & Sell SECOND HAND GOODS and also get Best Jobs and Properties on OIG MOBILE APP\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    e.toString();
                }
                break;
        }
    }

    private void init() {

        tvProname.setText(L.capSentences(productsList.getProductName()));
        tvRate.setText("Rs." + productsList.getRate());
        tvDesc.setText(L.capSentences(productsList.getProductDesc()));
        tvSku.setText(productsList.getSkuCode());
        qsize = productsList.getProductImages().size();

        if (qsize == 0) {
            TvCount.setText((pagerProd.getCurrentItem()) + "/ " + qsize);
            imgRight.setImageResource(R.drawable.unselectright);
        } else {
            if (qsize == 1) {
                imgRight.setImageResource(R.drawable.unselectright);
                TvCount.setText((pagerProd.getCurrentItem() + 1) + "/ " + qsize);
            } else {
                TvCount.setText((pagerProd.getCurrentItem() + 1) + "/ " + qsize);
                imgRight.setImageResource(R.drawable.slider_right_btn);
            }
        }
        pagerProd.setAdapter(new PagerSliderAdapter(ProductDetailsActivity.this, productsList.getProductImages()));

    }

    private void addProductInquiry() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.addProductsInq(ProID), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void deleteProduct() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ProductID, String.valueOf(ProID));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.deleteProduct(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Toast.makeText(this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(ProductDetailsActivity.this, Dashboard.class)
                            .putExtra(Constant.selectTab, 1).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }
        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }
}