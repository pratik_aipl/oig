package com.oig.test.activity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.model.MyInquiryModel;
import com.oig.test.activity.util.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyInquiryAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<MyInquiryModel> inquiryList;

    public MyInquiryAdpater(Context context, List<MyInquiryModel> inquiryLists) {
        this.context = context;
        this.inquiryList = inquiryLists;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.myinq_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        MyInquiryModel myInquiry = inquiryList.get(position);

        holder.tvProname.setText(myInquiry.getProductName());
        holder.tvDesc.setText(myInquiry.getProductDesc());
        holder.tvInqid.setText(myInquiry.getInquiryNo());
        holder.tvDate.setText(myInquiry.getInquiryDate());
        L.loadImageWithPicasso(context, myInquiry.getImageURL(), holder.imgInqimg, null);

    }

    @Override
    public int getItemCount() {
        return inquiryList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_proname)
        TextView tvProname;
        @BindView(R.id.tv_inqid)
        TextView tvInqid;
        @BindView(R.id.tv_desc)
        TextView tvDesc;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.img_Inqimg)
        ImageView imgInqimg;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}