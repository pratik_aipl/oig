package com.oig.test.activity.model;

import java.io.Serializable;

public class DetailModel implements Serializable {

    String labelName;
    String dataValue;

    public DetailModel(String labelName, String dataValue) {
        this.labelName = labelName;
        this.dataValue = dataValue;
    }

    public String getLabelName() {
        return labelName;
    }


    public String getDataValue() {
        return dataValue;
    }

}
