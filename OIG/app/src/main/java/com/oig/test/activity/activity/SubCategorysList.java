package com.oig.test.activity.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.NonSwipeableViewPager;
import com.oig.test.activity.adapter.AdPagerAdpater;
import com.oig.test.activity.adapter.SubCategoryAdpater;
import com.oig.test.activity.model.CategoryModel;
import com.oig.test.activity.util.Constant;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SubCategorysList extends AppCompatActivity {

   // private static int IMAGE_TIME_OUT = 6500;
   private static final String TAG = "SubCategorysList";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.recycler_list)
    RecyclerView catlist;
    @BindView(R.id.img_closeads)
    ImageView imgCloseads;
    @BindView(R.id.rel_flliper)
    RelativeLayout relFlliper;
    @BindView(R.id.pager_ads)
    NonSwipeableViewPager pagerAds;


    SubCategoryAdpater subCategoryAdpater;
    CategoryModel categoryModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categorys_list);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate: >>SubCategory");

        categoryModel = (CategoryModel) getIntent().getSerializableExtra(Constant.category);
        tvTittle.setText(categoryModel.getCategoryName());

        catlist.setHasFixedSize(true);
        catlist.setItemAnimator(new DefaultItemAnimator());
        catlist.setLayoutManager(new LinearLayoutManager(this));
        subCategoryAdpater = new SubCategoryAdpater(this, categoryModel.getSubCategories(), categoryModel.getCategoryID());
        catlist.setAdapter(subCategoryAdpater);

        if (categoryModel.getAdvertisementModel() != null && categoryModel.getAdvertisementModel().size() > 0) {
            relFlliper.setVisibility(View.VISIBLE);

            pagerAds.setAdapter(new AdPagerAdpater(SubCategorysList.this, categoryModel.getAdvertisementModel()));
            final int min = 0;
            final int max = categoryModel.getAdvertisementModel().size();
            final int random = new Random().nextInt((max - min) + 1) + min;
            pagerAds.setCurrentItem(random);

          /*  new Handler().postDelayed(() -> {
                relFlliper.setVisibility(View.GONE);
            }, IMAGE_TIME_OUT);*/

        } else {
            relFlliper.setVisibility(View.GONE);
        }


    }

    @OnClick({R.id.img_back, R.id.img_closeads, R.id.rel_flliper})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_closeads:
                relFlliper.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
