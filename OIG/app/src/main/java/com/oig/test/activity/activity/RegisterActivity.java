package com.oig.test.activity.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.model.LocationModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.network.RestApi;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;
import com.oig.test.activity.util.Utility;
import com.oig.test.activity.util.Validation;
import com.oig.test.activity.util.spinnerdatepicker.DatePicker;
import com.oig.test.activity.util.spinnerdatepicker.DatePickerDialog;
import com.oig.test.activity.util.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

import static com.oig.test.activity.util.ImagePathUtils.getFilePath;
import static com.oig.test.activity.util.L.onCaptureImageResult;

public class RegisterActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, DatePickerDialog.OnDateSetListener {

    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_surname)
    EditText edtSurname;
    @BindView(R.id.edt_mobileno)
    EditText edtMobileno;
    @BindView(R.id.rd_buyer)
    RadioButton radioButton;
    @BindView(R.id.rd_seller)
    RadioButton radioButton2;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.spn_gender)
    Spinner spnGender;
    @BindView(R.id.spn_location)
    Spinner spnLocation;
    @BindView(R.id.tv_dob)
    TextView tvDob;
    final Calendar myCalendar = Calendar.getInstance();
    @BindView(R.id.img_Profile)
    CircleImageView imgProfile;
    String SelectedDOD = null;
    @BindView(R.id.ll_dob)
    RelativeLayout llDob;

    List<LocationModel> sex = new ArrayList<>();
    List<LocationModel> location = new ArrayList<>();

    SexAdapter sexAdapter;
    LocationAdapter locationAdapter;

    String userChoosenTask, gender, usertype = "Buyer Dashboard", locationId;
    Subscription subscription;

    File imgFile = null;
    String mCurrentPhotoPath;
    DatePickerDialog pickerDialogBuilder;
    @BindView(R.id.li_gender)
    LinearLayout liGender;
    @BindView(R.id.li_location)
    LinearLayout liLocation;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.chk_condition)
    CheckBox chkCondition;
    @BindView(R.id.tv_tc)
    TextView tvTc;
    String regex = "[a-z]{3,30}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        if (L.isNetworkAvailable(RegisterActivity.this))
            getLocation();

        tvTc.setPaintFlags(tvTc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        sex.add(new LocationModel("Male", 1));
        sex.add(new LocationModel("Female", 2));
        sex.add(new LocationModel("Gender", 3));


        spnLocation.setOnItemSelectedListener(RegisterActivity.this);

        locationAdapter = new LocationAdapter(this, location);
        spnLocation.setAdapter(locationAdapter);


        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        pickerDialogBuilder = new SpinnerDatePickerDialogBuilder()
                .context(this)
                .callback(this)
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(year, month, day)
                .maxDate(year, month, day)
                .build();

        sexAdapter = new SexAdapter(this, sex);
        spnGender.setAdapter(sexAdapter);


        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                    radioButton = findViewById(checkedId);
                    usertype = radioButton.getText().toString();
                }
        );

        liGender.setOnClickListener(v -> {
            if (spnGender.getSelectedItem() == null) {
                spnGender.performClick();
            }
        });
        liLocation.setOnClickListener(v -> {
            if (spnLocation.getSelectedItem() != null) {
                spnLocation.performClick();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        if (position != 0)
            locationId = "" + location.get(position - 1).getID();
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @OnClick({R.id.btn_submit, R.id.tv_login, R.id.img_Profile, R.id.tv_dob, R.id.tv_tc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                validation();
                break;
            case R.id.tv_login:
                startActivity(new Intent(this, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case R.id.tv_dob:
                pickerDialogBuilder.show();
                break;
            case R.id.img_Profile:
                selectImage();
                break;
            case R.id.tv_tc:
                startActivity(new Intent(this, TermsCondition.class));
                break;
        }
    }


    public void validation() {
        if (Validation.isEmpty(L.getEditText(edtName))) {
            edtName.setError("Please enter Name");
        } else if ((L.getEditText(edtName).length() < 3)) {
            edtName.setError("Please enter Minimum 3 Character");
        } else if (Validation.isEmpty(L.getEditText(edtSurname))) {
            edtSurname.setError("Please enter Surname");
        } else if ((L.getEditText(edtSurname).length() < 3)) {
            edtSurname.setError("Please enter Minimum 3 Character");
        } else if (Validation.isEmpty(L.getEditText(edtMobileno))) {
            edtMobileno.setError("Please enter Mobile No");
        } else if (!Validation.isValidPhoneNumber(edtMobileno.getText())) {
            edtMobileno.setError("Check Mobile No");
        } else if (!TextUtils.isEmpty(edtEmail.getText()) && !Validation.isValidEmail(edtEmail.getText())) {
            edtEmail.setError("Check Email ID");
        } else if (sex.size() == 0 || spnGender.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please Select Gender", Toast.LENGTH_SHORT).show();
        } else if (SelectedDOD == null) {
            Toast.makeText(this, "Please Select DOB", Toast.LENGTH_SHORT).show();
        } else if (location.size() == 0 || spnLocation.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please Select Location", Toast.LENGTH_SHORT).show();
        } else if (!chkCondition.isChecked()) {
            Toast.makeText(this, "Please Agree Terms & Conditions", Toast.LENGTH_SHORT).show();
        } else {
            if (L.isNetworkAvailable(RegisterActivity.this)) {
                if (spnGender.getSelectedItemPosition() == 1) {
                    gender = "Male";
                } else {
                    gender = "Female";
                }
                ConfirmUser();
            }
        }
    }


    public void ConfirmUser() {
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Constant.USERName, RestApi.createRequestBody(L.getEditText(edtName)));
        map.put(Constant.SURNAME, RestApi.createRequestBody(L.getEditText(edtSurname)));
        map.put(Constant.MOBILE, RestApi.createRequestBody(L.getEditText(edtMobileno)));
        map.put(Constant.EMAIL, RestApi.createRequestBody(L.getEditText(edtEmail)));
        map.put(Constant.GENDER, RestApi.createRequestBody(gender));
        map.put(Constant.DOB, RestApi.createRequestBody(SelectedDOD));
        map.put(Constant.LOCATION_ID, RestApi.createRequestBody(locationId));
        //    map.put(Constant.DASHBOARD_PREF, RestApi.createRequestBody(usertype));
        MultipartBody.Part body = null;

        if (imgFile != null && !TextUtils.isEmpty(imgFile.getAbsolutePath())) {
            RequestBody requestFile = null;
            try {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new Compressor(this).compressToFile(imgFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
            body = MultipartBody.Part.createFormData(Constant.ProfileImage, imgFile.getName(), requestFile);
        }

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getUserRegVerify(map, body), (data) -> {
            showProgress(false);


            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject phone = jsonResponse.getJSONObject(Constant.data);
                    String otp = phone.getString("OTP");
                    Intent intent = new Intent(RegisterActivity.this, VerificationActivity.class);
                    intent.putExtra(Constant.Otp, otp);
                    intent.putExtra(Constant.MOBILENO, L.getEditText(edtMobileno));
                    intent.putExtra(Constant.IsFrom, "Register");

                    intent.putExtra(Constant.USERName, L.getEditText(edtName));
                    intent.putExtra(Constant.SURNAME, L.getEditText(edtSurname));
                    intent.putExtra(Constant.MOBILE, L.getEditText(edtMobileno));
                    intent.putExtra(Constant.EMAIL, L.getEditText(edtEmail));
                    intent.putExtra(Constant.GENDER, gender);
                    intent.putExtra(Constant.DOB, SelectedDOD);
                    intent.putExtra(Constant.LOCATION_ID, locationId);
                    //  intent.putExtra(Constant.DASHBOARD_PREF, usertype);
                    if (imgFile != null) {
                        intent.putExtra(Constant.Img_Path, imgFile.getAbsolutePath());
                    } else {
                        intent.putExtra(Constant.Img_Path, "");
                    }

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (data.code() == 206) {
                Toast.makeText(this, "Mobile No Already Registered...", Toast.LENGTH_SHORT).show();
            } else {
                L.serviceStatusFalseProcess(this, data);
            }
        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    public void getLocation() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLocation(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    location.clear();
                    location.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("City").toString(), LocationModel.class));
                    locationAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            boolean result = Utility.checkPermission(RegisterActivity.this);

            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                if (result)
                    cameraIntent();
            } else if (items[item].equals("Choose from Library")) {
                userChoosenTask = "Choose from Library";
                if (result)
                    galleryIntent();
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                imgFile = createImageFile();
                if (imgFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            imgFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            } catch (Exception ex) {
                Toast.makeText(this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(imgFile.getAbsolutePath(), imgProfile);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imgFile = new File(Objects.requireNonNull(getFilePath(this, data.getData())));
        imgProfile.setImageBitmap(bm);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        tvDob.setText(new StringBuilder().append(dayOfMonth).append("/")
                .append((monthOfYear + 1)).append("/").append(year));
        SelectedDOD = tvDob.getText().toString();

    }
}


class SexAdapter extends BaseAdapter {
    List<LocationModel> gender;
    LayoutInflater inflter;
    Context context;

    public SexAdapter(Context context, List<LocationModel> sex) {
        this.context = context;
        this.gender = sex;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        int count = gender.size() + 1;
        return count > 0 ? count - 1 : count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Gender");
            names.setTextColor(Color.GRAY);
        } else {
            LocationModel locationModel = gender.get(position - 1);
            names.setText(locationModel.getValue());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class LocationAdapter extends BaseAdapter {
    List<LocationModel> locationModels;
    LayoutInflater inflter;
    Context context;

    public LocationAdapter(Context context, List<LocationModel> locationModels) {
        this.context = context;
        this.locationModels = locationModels;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return locationModels.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return locationModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Location");
            names.setTextColor(Color.GRAY);
        } else {
            LocationModel locationModel = locationModels.get(position - 1);
            names.setText(locationModel.getValue());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}
