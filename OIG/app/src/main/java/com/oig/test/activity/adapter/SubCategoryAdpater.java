package com.oig.test.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.activity.ProductsListActivity;
import com.oig.test.activity.model.CategoryModel;
import com.oig.test.activity.util.Constant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SubCategoryAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<CategoryModel> subCategories;
    int CatId;

    public SubCategoryAdpater(Context context, List<CategoryModel> subCategories, int categoryID) {
        this.context = context;
        this.subCategories = subCategories;
        this.CatId = categoryID;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CategoryModel dashmodel = subCategories.get(position);
        if (dashmodel != null) {
            holder.tv_SubCatName.setText(dashmodel.getCategoryName());
        }
        holder.itemView.setOnClickListener(v -> {

            context.startActivity(new Intent(context, ProductsListActivity.class)
                    .putExtra("name", dashmodel.getCategoryName())
                    .putExtra(Constant.CatId, CatId)
                    .putExtra(Constant.SubID, dashmodel.getCategoryID())
                    .putExtra(Constant.CatType, "OLD"));
        });
    }

    @Override
    public int getItemCount() {
        return subCategories.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_SubCatName)
        TextView tv_SubCatName;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}