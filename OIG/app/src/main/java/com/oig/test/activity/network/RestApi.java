package com.oig.test.activity.network;

import android.support.annotation.NonNull;

import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface RestApi {

    @FormUrlEncoded
    @POST("location/city")
    Observable<Response<String>> getLocation(@FieldMap Map<String, String> stringMap);

    @Multipart
    @POST("auth/app_register")
    Observable<Response<String>> getRegister(@PartMap Map<String, RequestBody> params,
                                             @Part MultipartBody.Part files);

    @Multipart
    @POST("auth/checkotp")
    Observable<Response<String>> getUserRegVerify(@PartMap Map<String, RequestBody> params,
                                                  @Part MultipartBody.Part files);

    @Multipart
    @POST("profile_update/profile")
    Observable<Response<String>> updateProfile(@PartMap Map<String, RequestBody> params,
                                               @Part MultipartBody.Part files);

    @FormUrlEncoded
    @POST("auth/login")
    Observable<Response<String>> getLogin(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("products/product_delete")
    Observable<Response<String>> deleteProduct(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("auth/confirmotp")
    Observable<Response<String>> getverification(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("auth/resendotp")
    Observable<Response<String>> getresendotp(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("categories/category")
    Observable<Response<String>> getcategory(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("categories/shop_category")
    Observable<Response<String>> getShopMallCat(@FieldMap Map<String, String> stringMap);

//    @FormUrlEncoded
//    @POST("products/add_product")
//    Observable<Response<String>> uploadProduct(@FieldMap Map<String, String> stringMap);

    @Multipart
    @POST("products/add_product")
    Observable<Response<String>> uploadProduct(@PartMap Map<String, RequestBody> params,
                                               @Part List<MultipartBody.Part> files);


    @FormUrlEncoded
    @POST("products/edit_product")
    Observable<Response<String>> editProduct(@FieldMap Map<String, String> stringMap);

    @GET("products/product")
    Observable<Response<String>> getProducts(@QueryMap Map<String, String> stringMap);


    @GET("shop_detail/shop/{Id}")
    Observable<Response<String>> getShopList(@Path("Id") int ShopCategoryId);

    @GET("products/product_inquiry/")
    Observable<Response<String>> addProductsInq(@Query("ProID") int ProId);

    @GET("products/fields/{Id}")
    Observable<Response<String>> getFiledList(@Path("Id") int SubCatId);

    @GET("dashboard/my_inquiry")
    Observable<Response<String>> getMyInquiry();

    @GET("logout_advt/lg_advt")
    Observable<Response<String>> getExitads();

    @GET("dashboard/seller")
    Observable<Response<String>> getBuyerInquiry();

    @GET("products/my_product")
    Observable<Response<String>> getMyProducts();

    @GET("settings/remove_account")
    Observable<Response<String>> removeAccount();

    @FormUrlEncoded
    @POST("auth/logout")
    Observable<Response<String>> getLogout(@FieldMap Map<String, String> stringMap);


    @FormUrlEncoded
    @POST("profile_update/setting_notification")
    Observable<Response<String>> notificationStatus(@FieldMap Map<String, String> stringMap);

    //    @Multipart
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @HTTP(method = "DELETE", path = "peep/", hasBody = true)
    Observable<Response<String>> peepDelete(@Body RequestBody object);

    String MULTIPART_FORM_DATA = "multipart/form-data";

    static RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), s);
    }

}
