package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class AdvertisementModel implements Serializable {

    boolean isSet = false;
    @JsonField
    String AdvtwebURL;
    @JsonField
    String AdvtImageURL;
    @JsonField
    int DisplayAdvt;

    public boolean isSet() {
        return isSet;
    }

    public void setSet(boolean set) {
        isSet = set;
    }

    public int getDisplayAdvt() {
        return DisplayAdvt;
    }

    public void setDisplayAdvt(int displayAdvt) {
        DisplayAdvt = displayAdvt;
    }

    public String getAdvtwebURL() {
        return AdvtwebURL;
    }

    public void setAdvtwebURL(String advtwebURL) {
        AdvtwebURL = advtwebURL;
    }

    public String getAdvtImageURL() {
        return AdvtImageURL;
    }

    public void setAdvtImageURL(String advtImageURL) {
        AdvtImageURL = advtImageURL;
    }
}
