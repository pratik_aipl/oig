package com.oig.test.activity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;
import com.oig.test.activity.util.Validation;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class LoginActivity extends BaseActivity {


    private static final String TAG = "LoginActivity";
    @BindView(R.id.edt_mobileno)
    EditText edtMobileno;
    @BindView(R.id.btn_next)
    Button btnNext;
    @BindView(R.id.tv_register)
    TextView tvRegister;

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if (BuildConfig.DEBUG)
            edtMobileno.setText("9978277570");
    }

    @OnClick({R.id.btn_next, R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                if (Validation.isEmpty(L.getEditText(edtMobileno))) {
                edtMobileno.setError("Please enter Mobile No");
            } else {
                if (L.isNetworkAvailable(LoginActivity.this))
                    callLogin();
            }
                break;
            case R.id.tv_register:
                startActivity(new Intent(this, RegisterActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
        }
    }


    public void callLogin() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MOBILENO, L.getEditText(edtMobileno));
        map.put(Constant.DeviceID, L.getDeviceId(LoginActivity.this));
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject phone = jsonResponse.getJSONObject(Constant.data);

                    Intent intent = new Intent(LoginActivity.this, VerificationActivity.class);
                    intent.putExtra(Constant.Otp, phone.getString("otp"));
                    intent.putExtra(Constant.MOBILENO, phone.getString("mobile"));
                    intent.putExtra(Constant.IsFrom, "Login");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }
}
