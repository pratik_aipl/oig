package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class CategoryModel implements Serializable {
    @JsonField
    int CategoryID;
    @JsonField
    String CategoryName;
    @JsonField
    String CategoryIconURL;
    @JsonField
    int ParentID;

    @JsonField
    List<CategoryModel> SubCategories;

    @JsonField(name = "Advt")
    List<AdvertisementModel> advertisementModel;

    public List<AdvertisementModel> getAdvertisementModel() {
        return advertisementModel;
    }

    public void setAdvertisementModel(List<AdvertisementModel> advertisementModel) {
        this.advertisementModel = advertisementModel;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getCategoryIconURL() {
        return CategoryIconURL;
    }

    public void setCategoryIconURL(String categoryIconURL) {
        CategoryIconURL = categoryIconURL;
    }

    public int getParentID() {
        return ParentID;
    }

    public void setParentID(int parentID) {
        ParentID = parentID;
    }

    public List<CategoryModel> getSubCategories() {
        return SubCategories;
    }

    public void setSubCategories(List<CategoryModel> subCategories) {
        SubCategories = subCategories;
    }
}
