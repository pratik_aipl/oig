package com.oig.test.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.activity.Dashboard;
import com.oig.test.activity.model.UserData;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.network.RestApi;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;
import com.oig.test.activity.util.Utility;
import com.oig.test.activity.util.Validation;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

import static com.oig.test.activity.util.ImagePathUtils.getFilePath;
import static com.oig.test.activity.util.L.onCaptureImageResult;

public class ProfileChangeActivity extends BaseActivity {

    @BindView(R.id.img_EDTProfile)
    CircleImageView imgEDTProfile;
    @BindView(R.id.edt_Pname)
    EditText edtPname;
    @BindView(R.id.edt_Psurname)
    EditText edtPsurname;
    @BindView(R.id.edt_Pmobileno)
    EditText edtPmobileno;
    @BindView(R.id.rd_buyer)
    RadioButton rdBuyer;
    @BindView(R.id.rd_seller)
    RadioButton rdSeller;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.btn_profileupdate)
    Button btnProfileupdate;
    @BindView(R.id.img_close)
    ImageView imgClose;

    Subscription subscription;

    String userChoosenTask;
    File imgFile = null;
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    String mCurrentPhotoPath;
    final String[] usertype = {"Buyer Dashboard"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userprofile);
        ButterKnife.bind(this);

        edtPname.setText(user.getUsername());
        edtPsurname.setText(user.getSurname());
        edtPmobileno.setText(user.getMobile());
        edtPmobileno.setEnabled(false);
        final RadioButton[] radioButton = {null};

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                    radioButton[0] = findViewById(checkedId);
                    usertype[0] = radioButton[0].getText().toString();
                }
        );

        if (user.getProfile_pic() != null) {
            L.loadImageWithPicasso(ProfileChangeActivity.this, user.getProfile_pic(), imgEDTProfile, null);
        }
    }

    @OnClick({R.id.img_EDTProfile, R.id.btn_profileupdate, R.id.img_close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_EDTProfile:
                selectImage();
                break;
            case R.id.btn_profileupdate:

                if (Validation.isEmpty(L.getEditText(edtPname))) {
                    edtPname.setError("Please enter FirstName");
                } else if ((L.getEditText(edtPname).length() < 3)) {
                    edtPname.setError("Please enter Minimum 3 Character");
                } else if (Validation.isEmpty(L.getEditText(edtPsurname))) {
                    edtPsurname.setError("Please enter LastName");
                } else if ((L.getEditText(edtPsurname).length() < 3)) {
                    edtPsurname.setError("Please enter Minimum 3 Character");
                } else {

                    ProfileChangeActivity.this.updateProfile(L.getEditText(edtPname), L.getEditText(edtPsurname));
                }

                break;
            case R.id.img_close:
                onBackPressed();
                break;
        }
    }

    private void updateProfile(String username, String surname) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Constant.USERName, RestApi.createRequestBody(username));
        map.put(Constant.SURNAME, RestApi.createRequestBody(surname));
        // map.put(Constant.DASHBOARD_PREF, RestApi.createRequestBody(type));
        MultipartBody.Part body = null;

        if (imgFile != null && !TextUtils.isEmpty(imgFile.getAbsolutePath())) {
            RequestBody requestFile = null;
            try {
                requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new Compressor(this).compressToFile(imgFile));
            } catch (IOException e) {
                e.printStackTrace();
            }
            body = MultipartBody.Part.createFormData(Constant.ProfileImage, imgFile.getName(), requestFile);
        }

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.updateProfile(map, body), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    Intent intent = new Intent(this, Dashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    //get image
    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            boolean result = Utility.checkPermission(ProfileChangeActivity.this);

            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                if (result)
                    cameraIntent();
            } else if (items[item].equals("Choose from Library")) {
                userChoosenTask = "Choose from Library";
                if (result)
                    galleryIntent();
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                imgFile = createImageFile();
                if (imgFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            imgFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            } catch (Exception ex) {
                Toast.makeText(this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(imgFile.getAbsolutePath(), imgEDTProfile);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imgFile = new File(Objects.requireNonNull(getFilePath(this, data.getData())));
        imgEDTProfile.setImageBitmap(bm);
    }
}
