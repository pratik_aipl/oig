package com.oig.test.activity.model;

import java.io.File;

public class ImageData {

    String imagePath;
    File file;


    public ImageData(String imagePath, File file) {
        this.imagePath = imagePath;
        this.file = file;
    }

    public String getImagePath() {
        return imagePath;
    }

    public File getFile() {
        return file;
    }
}
