package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class MyInquiryModel implements Serializable {

    @JsonField
    String InquiryNo;
    @JsonField
    int ProductID;
    @JsonField
    String ProductName;
    @JsonField
    String ProductDesc;
    @JsonField
    String InquiryDate;
    @JsonField
    String ImageURL;

    public String getInquiryNo() {
        return InquiryNo;
    }

    public void setInquiryNo(String inquiryNo) {
        InquiryNo = inquiryNo;
    }

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int productID) {
        ProductID = productID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductDesc() {
        return ProductDesc;
    }

    public void setProductDesc(String productDesc) {
        ProductDesc = productDesc;
    }

    public String getInquiryDate() {
        return InquiryDate;
    }

    public void setInquiryDate(String inquiryDate) {
        InquiryDate = inquiryDate;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }
}
