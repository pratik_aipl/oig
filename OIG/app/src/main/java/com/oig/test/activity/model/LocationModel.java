package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class LocationModel implements Serializable {

    @JsonField
    String Value;
    @JsonField
    int ID;

    public LocationModel() {
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public LocationModel(String Value, int ID) {
        this.Value = Value;
        this.ID = ID;
    }
}
