package com.oig.test.activity.notificationreciver;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.oig.test.activity.activity.SplashActivity;
import com.oig.test.activity.activity.UserPrefActivity;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;


public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    private static final String TAG = "NotificationOpenedHandl";
    Context context;

    public NotificationOpenedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        Log.d(TAG, "notificationOpened: " + result.toString());
        OSNotificationAction.ActionType actionType = result.action.type;
        //JSONObject data = result.notification.payload.additionalData;
        JSONObject data = null;
        try {
            data = result.toJSONObject().getJSONObject("notification").getJSONObject("payload").getJSONObject("additionalData");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d(TAG, "data:::" + data.toString());
        if (data.has("action") && L.isLogin(context)) {
            try {
                if (data.getString("action").equalsIgnoreCase("admin")) {
                    Intent intent = new Intent(context, UserPrefActivity.class);
                    intent.putExtra(Constant.IsFrom, true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, SplashActivity.class);
                    intent.putExtra(Constant.IsFrom, true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    context.startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(context, "Something to wrong", Toast.LENGTH_SHORT).show();
        }

        if (actionType == OSNotificationAction.ActionType.ActionTaken)
            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);
        // The following can be used to open an Activity of your choice.
        // Replace - getApplicationContext() - with any Android Context.
     /*   if (L.isLogin(context)) {
            Intent intent = new Intent(context, SplashActivity.class);
            intent.putExtra(Constant.IsFrom, true);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        }*/
    }
}
