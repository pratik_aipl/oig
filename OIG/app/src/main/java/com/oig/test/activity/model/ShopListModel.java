package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class ShopListModel implements Serializable {


    @JsonField
    int DisplayAdvt;
    @JsonField
    List<AdvertisementModel> Advt;
    @JsonField
    List<ShopModel> Shops;
    @JsonField
    List<ProductsList> Products;

    public int getDisplayAdvt() {
        return DisplayAdvt;
    }

    public void setDisplayAdvt(int displayAdvt) {
        DisplayAdvt = displayAdvt;
    }

    public List<ProductsList> getProducts() {
        return Products;
    }

    public void setProducts(List<ProductsList> products) {
        Products = products;
    }

    public List<AdvertisementModel> getAdvt() {
        return Advt;
    }

    public void setAdvt(List<AdvertisementModel> advt) {
        Advt = advt;
    }

    public List<ShopModel> getShops() {
        return Shops;
    }

    public void setShops(List<ShopModel> shops) {
        Shops = shops;
    }
}
