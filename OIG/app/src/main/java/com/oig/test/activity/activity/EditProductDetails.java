package com.oig.test.activity.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.model.DetailModel;
import com.oig.test.activity.model.ProductsList;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class EditProductDetails extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.edt_prodname)
    EditText edtProdname;
    @BindView(R.id.edt_desc)
    EditText edtDesc;
    @BindView(R.id.li_requird)
    LinearLayout liRequird;
    @BindView(R.id.btn_create)
    Button btnCreate;

    ProductsList productsList;
    @BindView(R.id.liMainview)
    LinearLayout liMainview;

    List<DetailModel> detailModels = new ArrayList<>();
    final List<EditText> allEds = new ArrayList<>();
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product_details);
        ButterKnife.bind(this);

        tvTittle.setText("Edit Product");

        productsList = (ProductsList) getIntent().getSerializableExtra(Constant.Products);

        detailModels.clear();
        detailModels.add(new DetailModel(productsList.getLabel1(), productsList.getFieldValue1()));
        detailModels.add(new DetailModel(productsList.getLabel2(), productsList.getFieldValue2()));
        detailModels.add(new DetailModel(productsList.getLabel3(), productsList.getFieldValue3()));
        detailModels.add(new DetailModel(productsList.getLabel4(), productsList.getFieldValue4()));
        detailModels.add(new DetailModel(productsList.getLabel5(), productsList.getFieldValue5()));
        detailModels.add(new DetailModel(productsList.getLabel6(), productsList.getFieldValue6()));
        detailModels.add(new DetailModel(productsList.getLabel7(), productsList.getFieldValue7()));
        detailModels.add(new DetailModel(productsList.getLabel8(), productsList.getFieldValue8()));
        detailModels.add(new DetailModel(productsList.getLabel9(), productsList.getFieldValue9()));
        detailModels.add(new DetailModel(productsList.getLabel10(), productsList.getFieldValue10()));

        if (productsList != null) {
            edtProdname.setText(productsList.getProductName());
            edtDesc.setText(productsList.getProductDesc());
            addView();
        }

    }

    @OnClick({R.id.img_back, R.id.btn_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_create:
                uploadProducts();
                break;
        }
    }

    public void addView() {
        for (int i = 0; i < productsList.getNofFields(); i++) {
            TextView textView = new TextView(this);
            EditText editText = new EditText(this);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams tp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final float scale = getResources().getDisplayMetrics().density;
            tp.height = (int) (15 * scale);
            p.height = (int) (50 * scale);
            allEds.add(editText);
            tp.setMargins((int) (11 * scale), (int) (7 * scale), 0, 0);
            p.setMargins(0, (int) (11 * scale), 0, 0);
            textView.setLayoutParams(tp);
            editText.setLayoutParams(p);
            textView.setText(detailModels.get(i).getLabelName());
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            editText.setHint(detailModels.get(i).getLabelName());
            editText.setText(detailModels.get(i).getDataValue());
            editText.setBackground(ContextCompat.getDrawable(this, R.drawable.edittextbox));
            editText.setTextColor(Color.BLACK);
            editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            editText.setMaxLines(1);
            editText.setSingleLine();
            editText.setPadding((int) (10 * scale), 0, 0, 0);
            liMainview.addView(textView);
            liMainview.addView(editText);
        }
    }

    public void uploadProducts() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.ProductID, String.valueOf(productsList.getProductID()));
        map.put(Constant.ProductDesc, L.getEditText(edtDesc));

        for (int i = 0; i < allEds.size(); i++) {
            map.put("FieldValue" + (i + 1), L.getEditText(allEds.get(i)));
        }

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.editProduct(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    startActivity(new Intent(EditProductDetails.this, Dashboard.class).putExtra(Constant.selectTab, 1));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }
}
