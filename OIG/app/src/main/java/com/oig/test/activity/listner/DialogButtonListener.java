package com.oig.test.activity.listner;

public interface DialogButtonListener {
    void onPositiveButtonClicked();
    void onNegativButtonClicked();
}