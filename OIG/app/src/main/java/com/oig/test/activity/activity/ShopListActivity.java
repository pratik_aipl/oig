package com.oig.test.activity.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.adapter.ShopListAdpater;
import com.oig.test.activity.model.ShopModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ShopListActivity extends BaseActivity {

    private static final String TAG = "ShopListActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.recycler_list)
    RecyclerView malllist;

    ShopListAdpater shopListAdpater;
    Subscription subscription;
    int ShopCategoryId;

    List<ShopModel> shopModels = new ArrayList<>();

    @BindView(R.id.img_comingsoon)
    ImageView imgComingsoon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);
        ButterKnife.bind(this);
        Log.d(TAG, "onCreate: >>Shoplist");

        tvTittle.setText(getIntent().getStringExtra(Constant.category));
        ShopCategoryId = getIntent().getIntExtra(Constant.CatId, 0);

        if (L.isNetworkAvailable(ShopListActivity.this)) {
            getShopList();
        }
        malllist.setHasFixedSize(true);
        malllist.setItemAnimator(new DefaultItemAnimator());
        malllist.setLayoutManager(new LinearLayoutManager(this));
        shopListAdpater = new ShopListAdpater(this, shopModels, ShopCategoryId);
        malllist.setAdapter(shopListAdpater);

    }


    @OnClick({R.id.img_back, R.id.tv_tittle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private void getShopList() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getShopList(ShopCategoryId), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.data)) {
                        if (jsonResponse.getJSONObject(Constant.data).has(Constant.Shops)) {

                            shopModels.clear();
                            shopModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.Shops).toString(), ShopModel.class));
                            shopListAdpater.notifyDataSetChanged();

                        } else {
                            imgComingsoon.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (data.code() == 400) {
                imgComingsoon.setVisibility(View.VISIBLE);
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
