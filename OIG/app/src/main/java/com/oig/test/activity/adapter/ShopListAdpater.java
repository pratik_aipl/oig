package com.oig.test.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oig.test.R;
import com.oig.test.activity.activity.ProductsListActivity;
import com.oig.test.activity.model.ShopModel;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ShopListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ShopModel> shopListModels;
    int CatId;

    public ShopListAdpater(Context context, List<ShopModel> shopListModels, int id) {
        this.context = context;
        this.shopListModels = shopListModels;
        this.CatId = id;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        ShopModel shopListModel = shopListModels.get(position);

        L.loadImageWithPicasso(context, shopListModel.getShopPic(), holder.img_Mall_cat, null);

        if (shopListModel.getIsPaid().equalsIgnoreCase("1")) {
            holder.imgpaid.setVisibility(View.VISIBLE);
            holder.img_Mall_cat.setOnClickListener(v -> context.startActivity(new Intent(context, ProductsListActivity.class)
                    .putExtra("name", shopListModel.getShopName())
                    .putExtra(Constant.CatId, CatId)
                    .putExtra(Constant.SubID, shopListModel.getUserID())
                    .putExtra(Constant.CatType, "NEW")));
        } else {
            holder.imgpaid.setVisibility(View.GONE);
        }
        holder.imgpaid.setOnClickListener(v -> context.startActivity(new Intent(context, ProductsListActivity.class)
                .putExtra("name", shopListModel.getShopName())
                .putExtra(Constant.CatId, CatId)
                .putExtra(Constant.SubID, shopListModel.getUserID())
                .putExtra(Constant.CatType, "NEW")));
    }

    @Override
    public int getItemCount() {
        return shopListModels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_Mall_cat)
        ImageView img_Mall_cat;
        @BindView(R.id.img_Paid)
        ImageView imgpaid;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}