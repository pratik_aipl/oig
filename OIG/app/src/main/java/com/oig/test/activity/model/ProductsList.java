package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class ProductsList implements Serializable {

    @JsonField
    int ProductID;
    @JsonField
    int NofFields;
    @JsonField
    String ProductName;
    @JsonField
    String ProductDesc;
    @JsonField
    String skuCode;
    @JsonField
    String Rate;
    @JsonField
    String LocationName;

    @JsonField
    String Label1;
    @JsonField
    String Label2;
    @JsonField
    String Label3;
    @JsonField
    String Label4;
    @JsonField
    String Label5;
    @JsonField
    String Label6;
    @JsonField
    String Label7;
    @JsonField
    String Label8;
    @JsonField
    String Label9;
    @JsonField
    String Label10;
    @JsonField
    String FieldValue1;
    @JsonField
    String FieldValue2;
    @JsonField
    String FieldValue3;
    @JsonField
    String FieldValue4;
    @JsonField
    String FieldValue5;
    @JsonField
    String FieldValue6;
    @JsonField
    String FieldValue7;
    @JsonField
    String FieldValue8;
    @JsonField
    String FieldValue9;
    @JsonField
    String FieldValue10;
    @JsonField
    String admin_number;
    @JsonField
    String mobile;
    @JsonField
    String IsPaid;

    String viewType = "";
    AdvertisementModel advertisementModel;

    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }

    public AdvertisementModel getAdvertisementModel() {
        return advertisementModel;
    }

    public void setAdvertisementModel(AdvertisementModel advertisementModel) {
        this.advertisementModel = advertisementModel;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    @JsonField(name = "Advt")
    List<AdvertisementModel> advts;

    public List<AdvertisementModel> getAdvts() {
        return advts;
    }

    public void setAdvts(List<AdvertisementModel> advts) {
        this.advts = advts;
    }

    public String getAdmin_number() {
        return admin_number;
    }

    public void setAdmin_number(String admin_number) {
        this.admin_number = admin_number;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIsPaid() {
        return IsPaid;
    }

    public void setIsPaid(String isPaid) {
        IsPaid = isPaid;
    }

    public int getNofFields() {
        return NofFields;
    }

    public void setNofFields(int nofFields) {
        NofFields = nofFields;
    }

    public String getLabel1() {
        return Label1;
    }

    public void setLabel1(String label1) {
        Label1 = label1;
    }

    public String getLabel2() {
        return Label2;
    }

    public void setLabel2(String label2) {
        Label2 = label2;
    }

    public String getLabel3() {
        return Label3;
    }

    public void setLabel3(String label3) {
        Label3 = label3;
    }

    public String getLabel4() {
        return Label4;
    }

    public void setLabel4(String label4) {
        Label4 = label4;
    }

    public String getLabel5() {
        return Label5;
    }

    public void setLabel5(String label5) {
        Label5 = label5;
    }

    public String getLabel6() {
        return Label6;
    }

    public void setLabel6(String label6) {
        Label6 = label6;
    }

    public String getLabel7() {
        return Label7;
    }

    public void setLabel7(String label7) {
        Label7 = label7;
    }

    public String getLabel8() {
        return Label8;
    }

    public void setLabel8(String label8) {
        Label8 = label8;
    }

    public String getLabel9() {
        return Label9;
    }

    public void setLabel9(String label9) {
        Label9 = label9;
    }

    public String getLabel10() {
        return Label10;
    }

    public void setLabel10(String label10) {
        Label10 = label10;
    }

    public String getFieldValue1() {
        return FieldValue1;
    }

    public void setFieldValue1(String fieldValue1) {
        FieldValue1 = fieldValue1;
    }

    public String getFieldValue2() {
        return FieldValue2;
    }

    public void setFieldValue2(String fieldValue2) {
        FieldValue2 = fieldValue2;
    }

    public String getFieldValue3() {
        return FieldValue3;
    }

    public void setFieldValue3(String fieldValue3) {
        FieldValue3 = fieldValue3;
    }

    public String getFieldValue4() {
        return FieldValue4;
    }

    public void setFieldValue4(String fieldValue4) {
        FieldValue4 = fieldValue4;
    }

    public String getFieldValue5() {
        return FieldValue5;
    }

    public void setFieldValue5(String fieldValue5) {
        FieldValue5 = fieldValue5;
    }

    public String getFieldValue6() {
        return FieldValue6;
    }

    public void setFieldValue6(String fieldValue6) {
        FieldValue6 = fieldValue6;
    }

    public String getFieldValue7() {
        return FieldValue7;
    }

    public void setFieldValue7(String fieldValue7) {
        FieldValue7 = fieldValue7;
    }

    public String getFieldValue8() {
        return FieldValue8;
    }

    public void setFieldValue8(String fieldValue8) {
        FieldValue8 = fieldValue8;
    }

    public String getFieldValue9() {
        return FieldValue9;
    }

    public void setFieldValue9(String fieldValue9) {
        FieldValue9 = fieldValue9;
    }

    public String getFieldValue10() {
        return FieldValue10;
    }

    public void setFieldValue10(String fieldValue10) {
        FieldValue10 = fieldValue10;
    }

    @JsonField
    List<ProductsImage> ProductImages;

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int productID) {
        ProductID = productID;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getProductDesc() {
        return ProductDesc;
    }

    public void setProductDesc(String productDesc) {
        ProductDesc = productDesc;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public List<ProductsImage> getProductImages() {
        return ProductImages;
    }

    public void setProductImages(List<ProductsImage> productImages) {
        ProductImages = productImages;
    }
}
