package com.oig.test.activity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {
  private static final String TAG = "SplashActivity";
  private static int SPLASH_TIME_OUT = 3500;
  @BindView(R.id.tv_version)
  TextView tvVersion;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);
    ButterKnife.bind(this);
    //int SPLASH_TIME_OUT = 1500;
    tvVersion.setText("App version " + BuildConfig.VERSION_NAME);
    new Handler().postDelayed(() -> {
      if (prefs.getBoolean(Constant.isLogin, false)) {
        Intent i;
        if (getIntent().getBooleanExtra(Constant.IsFrom, false)) {
          i = new Intent(this, Dashboard.class);
          i.putExtra(Constant.ChildTab, true);
          i.putExtra(Constant.tab, getIntent().getIntExtra(Constant.tab, 1));
          i.putExtra(Constant.selectTab, (getIntent().getBooleanExtra(Constant.IsFrom, false) ? 1 : 0));
        } else if (getIntent().getBooleanExtra(Constant.admin, false)) {
          i = new Intent(this, Dashboard.class);
          i.putExtra(Constant.actindex, getIntent().getIntExtra(Constant.selectindex, 0));
          i.putExtra(Constant.ChildTab, getIntent().getBooleanExtra(Constant.ChildTab, false));
          i.putExtra(Constant.tab, getIntent().getIntExtra(Constant.tab, 0));
          i.putExtra(Constant.selectTab, 20);
        } else {
          i = new Intent(this, UserPrefActivity.class);
        }
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
      } else {
        Intent i = new Intent(this, WelcomeActivity.class);
        startActivity(i);
      }
      finish();
    }, SPLASH_TIME_OUT);

  }
}

