package com.oig.test.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.activity.ShopListActivity;
import com.oig.test.activity.model.CategoryModel;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ShopMallCatAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<CategoryModel> ShopCategoryModels;

    public ShopMallCatAdpater(Context context, List<CategoryModel> ShopCategoryModels) {
        this.context = context;
        this.ShopCategoryModels = ShopCategoryModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.shop_mall_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        CategoryModel ShopCategoryModel = ShopCategoryModels.get(position);

        holder.tv_Mall_cat.setText(ShopCategoryModel.getCategoryName());
        L.loadImageWithPicasso(context, ShopCategoryModel.getCategoryIconURL(), holder.img_Mall_cat, null);

        holder.itemView.setOnClickListener(v -> {

            context.startActivity(new Intent(context, ShopListActivity.class)
                    .putExtra(Constant.CatId, ShopCategoryModel.getCategoryID())
                    .putExtra(Constant.category, ShopCategoryModel.getCategoryName()));

        });
    }

    @Override
    public int getItemCount() {
        return ShopCategoryModels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_Mall_cat)
        ImageView img_Mall_cat;
        @BindView(R.id.tv_Mall_cat)
        TextView tv_Mall_cat;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}