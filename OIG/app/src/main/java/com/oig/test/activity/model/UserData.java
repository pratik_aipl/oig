package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class UserData implements Serializable {

    @JsonField
    String id;

    public String getIsPaid() {
        return IsPaid;
    }

    public void setIsPaid(String isPaid) {
        IsPaid = isPaid;
    }

    @JsonField
    String IsPaid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonField
    String username;
    @JsonField
    String surname;
    @JsonField
    String mobile;
    @JsonField
    String email;
    @JsonField
    String dob;
    @JsonField
    String profile_pic;
    @JsonField
    String dashboard_pref;
    @JsonField
    String NotificationStatus;

    public String getNotificationStatus() {
        return NotificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        NotificationStatus = notificationStatus;
    }

    public UserData() {
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDashboard_pref() {
        return dashboard_pref;
    }

    public void setDashboard_pref(String dashboard_pref) {
        this.dashboard_pref = dashboard_pref;
    }
}
