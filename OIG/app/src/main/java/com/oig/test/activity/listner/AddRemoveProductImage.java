package com.oig.test.activity.listner;

public interface AddRemoveProductImage {
     void onAddImage(int pos);
     void onRemoveImage(int pos);
}
