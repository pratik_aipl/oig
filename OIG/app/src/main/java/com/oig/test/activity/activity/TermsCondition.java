package com.oig.test.activity.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsCondition extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    String url = "http://myoig.com/web_assets/pdf/term_condition.pdf";
    public WebView web_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        ButterKnife.bind(this);
        tvTittle.setText("Terms & Conditions");
        web_view = findViewById(R.id.web_view);

        WebSettings settings = web_view.getSettings();
        settings.setAllowFileAccess(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setAllowUniversalAccessFromFileURLs(false);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(false);
        settings.setDomStorageEnabled(true);
        web_view.setWebViewClient(new myWebClient());

        //   String extantion = String.valueOf(url.lastIndexOf("."));
    /*    if (!extantion.equals(".pdf")) {
            Toast.makeText(this, "Preview not available in app, To open it please download.", Toast.LENGTH_SHORT).show();
        } else {*/
        web_view.loadUrl("https://docs.google.com/gview?embedded=true&url=" + url);
        //  }
    }

    @OnClick({R.id.img_back, R.id.tv_tittle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    private class myWebClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            view.loadUrl(url);
            return true;

        }

        public void onLoadResource(WebView view, String url) {

        }

        @Override
        public void onReceivedError(WebView view, int errorCode,
                                    String description, String failingUrl) {
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            try {
                showProgress(false);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
}
