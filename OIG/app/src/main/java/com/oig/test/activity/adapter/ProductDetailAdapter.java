package com.oig.test.activity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.model.DetailModel;
import com.oig.test.activity.util.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProductDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<DetailModel> detailModelList;

    public ProductDetailAdapter(Context context, List<DetailModel> detailModelList) {
        this.context = context;
        this.detailModelList = detailModelList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_details_item, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        DetailModel detailModel = detailModelList.get(position);

        holder.mLabel.setText(detailModel.getLabelName());
        holder.mData.setText(detailModel.getDataValue());
    }

    @Override
    public int getItemCount() {
        return detailModelList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mLabel)
        TextView mLabel;

        @BindView(R.id.mData)
        TextView mData;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}