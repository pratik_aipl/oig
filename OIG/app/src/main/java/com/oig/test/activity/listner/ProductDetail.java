package com.oig.test.activity.listner;

import com.oig.test.activity.model.ProductsList;

public interface ProductDetail {
     void proddetail(ProductsList productsImages);
}
