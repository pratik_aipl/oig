package com.oig.test.activity.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.R;
import com.oig.test.activity.activity.ProductDetailsActivity;
import com.oig.test.activity.activity.UploadProductsActivity;
import com.oig.test.activity.adapter.ProductListAdpater;
import com.oig.test.activity.listner.ProductDetail;
import com.oig.test.activity.model.ProductsList;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import rx.Subscription;

public class MyProductFragment extends BaseFragment implements ProductDetail {

    List<ProductsList> myProductsList = new ArrayList<>();
    ProductListAdpater myProductListAdpater;

    Subscription subscription;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layoutView = inflater.inflate(R.layout.buyerinquiryfrag, container, false);
        ButterKnife.bind(this, layoutView);

        RecyclerView recyclerList = layoutView.findViewById(R.id.recycler_list);

        FloatingActionButton fabAdd = layoutView.findViewById(R.id.fab_add);
        fabAdd.setOnClickListener(v -> getContext().startActivity(new Intent(getActivity(), UploadProductsActivity.class)));

        if (L.isNetworkAvailable(getActivity()))
            getProducts();

        recyclerList.setHasFixedSize(true);
        recyclerList.setItemAnimator(new DefaultItemAnimator());
        recyclerList.setLayoutManager(new LinearLayoutManager(getActivity()));

        myProductListAdpater = new ProductListAdpater(getActivity(), this, myProductsList);
        recyclerList.setAdapter(myProductListAdpater);

        return layoutView;
    }

    private void getProducts() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getMyProducts(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data))
                        if (jsonResponse.getJSONObject(Constant.data).has(Constant.Products)) {
                            myProductsList.clear();
                            myProductsList.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.Products).toString(), ProductsList.class));
                            myProductListAdpater.notifyDataSetChanged();
                        } else {
                        }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }


    @Override
    public void proddetail(ProductsList productsImages) {

        startActivity(new Intent(getActivity(), ProductDetailsActivity.class)
                .putExtra(Constant.Products, productsImages)
                .putExtra(Constant.ProID, productsImages.getProductID())
                .putExtra(Constant.IsFrom, "MyProduct").setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}