package com.oig.test.activity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.adapter.ProductListAdpater;
import com.oig.test.activity.listner.ProductDetail;
import com.oig.test.activity.model.AdvertisementModel;
import com.oig.test.activity.model.ProductsList;
import com.oig.test.activity.model.ShopListModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ProductsListActivity extends BaseActivity implements ProductDetail {

    private static final String TAG = "ProductsListActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerList;
    ProductListAdpater productListAdpater;
    @BindView(R.id.img_filter)
    ImageView imgFilter;

    Subscription subscription;
    List<ProductsList> productsLists = new ArrayList<>();
    List<AdvertisementModel> advertisementModels = new ArrayList<>();

    ShopListModel ProductModel;
    int CategoryId, SubCategoryId;
    String CatType;

    @BindView(R.id.li_notfound)
    LinearLayout liNotfound;
    @BindView(R.id.img_edit)
    ImageView imgEdit;
    @BindView(R.id.searchView)
    SearchView search_View;
    @BindView(R.id.lli)
    LinearLayout liHeader;
    @BindView(R.id.img_closesrc)
    ImageView imgClosesrc;
    @BindView(R.id.li)
    LinearLayout li;
    @BindView(R.id.rel_search)
    RelativeLayout relSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);
        ButterKnife.bind(this);

        tvTittle.setText(getIntent().getStringExtra("name"));

        CategoryId = getIntent().getIntExtra(Constant.CatId, 0);
        SubCategoryId = getIntent().getIntExtra(Constant.SubID, 0);
        CatType = getIntent().getStringExtra(Constant.CatType);

        recyclerList.setHasFixedSize(true);
        recyclerList.setItemAnimator(new DefaultItemAnimator());
        recyclerList.setLayoutManager(new LinearLayoutManager(this));

        productListAdpater = new ProductListAdpater(this, this, productsLists);
        recyclerList.setAdapter(productListAdpater);
        if (L.isNetworkAvailable(this))
            getProducts();

        imgFilter.setVisibility(View.VISIBLE);
        imgEdit.setVisibility(View.GONE);

    }

    @OnClick({R.id.img_back, R.id.tv_tittle, R.id.img_filter, R.id.img_closesrc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.img_closesrc:
                liHeader.setVisibility(View.VISIBLE);
                relSearch.setVisibility(View.GONE);
                break;
            case R.id.img_filter:
                liHeader.setVisibility(View.GONE);
                relSearch.setVisibility(View.VISIBLE);
                search_View.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        productListAdpater.getFilter().filter(query);
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        if (newText.isEmpty()) {
                            imgClosesrc.setVisibility(View.VISIBLE);
                        } else {
                            imgClosesrc.setVisibility(View.GONE);
                        }
                        productListAdpater.getFilter().filter(newText);

                        return false;
                    }
                });
                break;
        }
    }

    private void getProducts() {
        showProgress(true);
        Map<String, String> map = new HashMap<>();
        if (CatType.equalsIgnoreCase("OLD")) {
            map.put("category_id", String.valueOf(SubCategoryId));
            map.put("product_type", CatType);
        } else {
            map.put("category_id", String.valueOf(CategoryId));
            map.put("ShopID", String.valueOf(SubCategoryId));
            map.put("product_type", CatType);
        }
        subscription = NetworkRequest.performAsyncRequest(restApi.getProducts(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        if (jsonResponse.getJSONObject(Constant.data).has(Constant.Products)) {
                            List<ProductsList> productsLists = new ArrayList<>();

                            ProductModel = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), ShopListModel.class);
                            productsLists.addAll(ProductModel.getProducts());

                            advertisementModels.clear();
                            advertisementModels.addAll(ProductModel.getAdvt());

                            if (advertisementModels != null && advertisementModels.size() > 0) {

                                for (int j = 0; j < productsLists.size(); j++) {
                                    AdvertisementModel advertisementModel = null;
                                    ProductsList productsList = productsLists.get(j);
                                    for (int i = 0; i < advertisementModels.size(); i++) {
                                        advertisementModel = advertisementModels.get(i);
                                        Log.d(TAG, "getProducts: " + advertisementModel.isSet());
                                        if (!advertisementModel.isSet() && j != 0 && j % ProductModel.getDisplayAdvt() == 0) {
                                            advertisementModel.setSet(true);
                                            productsList.setAdvertisementModel(advertisementModel);
                                            productsList.setViewType("ad");
                                            break;
                                        }
                                    }
                                    productsLists.set(j, productsList);
                                }
                            }

                            this.productsLists.addAll(productsLists);
                            productListAdpater.notifyDataSetChanged();
                        } else {
                            recyclerList.setVisibility(View.GONE);
                            liNotfound.setVisibility(View.VISIBLE);
                            imgFilter.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (data.code() == 400) {
                recyclerList.setVisibility(View.GONE);
                liNotfound.setVisibility(View.VISIBLE);
                imgFilter.setVisibility(View.GONE);

            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    @Override
    public void proddetail(ProductsList productsLists) {

        startActivity(new Intent(this, ProductDetailsActivity.class)
                .putExtra(Constant.Products, productsLists)
                .putExtra(Constant.ProID, productsLists.getProductID())
                .putExtra(Constant.IsFrom, CatType));

    }

    @Override
    public void onBackPressed() {
        if (relSearch.getVisibility() == View.VISIBLE) {
            liHeader.setVisibility(View.VISIBLE);
            relSearch.setVisibility(View.GONE);
            search_View.setQuery("", false);
            search_View.clearFocus();
        } else {
            super.onBackPressed();
        }
    }
}
