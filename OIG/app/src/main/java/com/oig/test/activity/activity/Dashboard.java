package com.oig.test.activity.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.ProfileChangeActivity;
import com.oig.test.activity.fragment.BuyerDashboardFragment;
import com.oig.test.activity.fragment.SellerDashboardFragment;
import com.oig.test.activity.model.AdvertisementModel;
import com.oig.test.activity.model.UserData;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.network.RestApi;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;
import com.oig.test.activity.util.Utility;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

import static com.oig.test.activity.util.ImagePathUtils.getFilePath;
import static com.oig.test.activity.util.L.onCaptureImageResult;

public class Dashboard extends BaseActivity {

    private static final String TAG = "Dashboard";

    ActionBarDrawerToggle t;

    @BindView(R.id.img_drawer)
    ImageView imgDrawer;
    @BindView(R.id.tv_dashboard)
    TextView tvDashboard;
    @BindView(R.id.framelayout)
    FrameLayout framelayout;
    @BindView(R.id.nv)
    NavigationView nv;
    @BindView(R.id.activity_main)
    DrawerLayout drawerLayout;

    TextView navUsername, navmobile;
    CircleImageView img_Nev_Profile;
    LinearLayout li_profile;
    public static String a;
    Dialog profiledialog;
    int selectTab = 0;
    boolean firstclick = false;

    Subscription subscription;

 /*   @BindView(R.id.img_burtoggle)
    TextView imgBurtoggle;
    @BindView(R.id.img_selltoggle)
    TextView imgSelltoggle;*/

    @BindView(R.id.li_exitads)
    LinearLayout liExitads;
    @BindView(R.id.img_ads)
    ImageView imgAds;
    @BindView(R.id.img_closeads)
    ImageView imgCloseads;
    @BindView(R.id.rel_ads)
    RelativeLayout relAds;
    List<AdvertisementModel> advertisementModels = new ArrayList<>();

    String userChoosenTask;
    File imgFile = null;
    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;
    String mCurrentPhotoPath;
    CircleImageView img_edtprofile;

    boolean ChildTab = false;
    @BindView(R.id.tv_chngdash)
    TextView tvChngdash;
    boolean sel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        Log.d(TAG, "onCreate: >>Dashboard");

        if (L.isNetworkAvailable(this))
            getExitads();

        drawerLayout.addDrawerListener(t);
        nv.setItemIconTintList(null);

        selectTab = getIntent().getIntExtra(Constant.selectTab, 0);
        ChildTab = getIntent().getBooleanExtra(Constant.ChildTab, false);
        imgCloseads.setOnClickListener(v -> {
            super.onBackPressed();
        });

        if (selectTab == 0 ) {
            selectItem(0);
        } else {
            selectItem(1);
        }


        tvChngdash.setOnClickListener(v -> {
            if (sel == false) {
                selectItem(1);
            } else {
                selectItem(0);
            }
        });

        a = user.getUsername();

        View headerView = nv.getHeaderView(0);
        navUsername = headerView.findViewById(R.id.tv_username);
        navmobile = headerView.findViewById(R.id.tv_usermob);
        img_Nev_Profile = headerView.findViewById(R.id.img_User);
        li_profile = headerView.findViewById(R.id.li_profile);
        navUsername.setText(user.getUsername());
        navmobile.setText(user.getMobile());

        if (user.getProfile_pic() != null) {
            L.loadImageWithPicasso(Dashboard.this, user.getProfile_pic(), img_Nev_Profile, null);
        }
        li_profile.setOnClickListener(v -> {
            drawerLayout.closeDrawer(Gravity.LEFT);
            // ProfileDialog(Dashboard.this);
            Intent myIntent = new Intent(Dashboard.this, ProfileChangeActivity.class);
            startActivity(myIntent);
        });

        nv.setNavigationItemSelectedListener(item -> {
            int id = item.getItemId();
            switch (id) {
                case R.id.buyerdashboard:
                    selectItem(0);
                    break;
                case R.id.sellerdashboard:
                    selectItem(1);
                    break;
                case R.id.shopmall:
                    selectItem(2);
                    break;
                case R.id.pastinquiry:
                    selectItem(3);
                    break;
                case R.id.myprofile:
                    selectItem(4);
                    break;
                case R.id.settings:
                    selectItem(5);
                    break;
                case R.id.contactus:
                    selectItem(6);
                    break;
                case R.id.aboutus:
                    selectItem(7);
                    break;
                case R.id.termandcondition:
                    selectItem(8);
                    break;
                case R.id.logout:
                    selectItem(9);
                    break;
                default:
                    break;
            }
            return true;

        });

        liExitads.setOnClickListener(v -> {

            if (advertisementModels.size() > 0) {
                String url = advertisementModels.get(0).getAdvtwebURL().contains("http") ? advertisementModels.get(0).getAdvtwebURL() : "http://" + advertisementModels.get(0).getAdvtwebURL();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (firstclick == false)
            exitalert();

    }

    private void selectItem(int position) {

        Fragment fragment = null;
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.selectTab, selectTab);
        bundle.putBoolean(Constant.TabIndex, ChildTab);

        switch (position) {
            case 0:
                fragment = new BuyerDashboardFragment();
                fragment.setArguments(bundle);
                tvDashboard.setText(getString(R.string.buyer_dashboard));
                //imgBurtoggle.setVisibility(View.GONE);
                //imgSelltoggle.setVisibility(View.VISIBLE);
                tvChngdash.setText("Sell / Upload My Products >");
                ChildTab = false;
                sel = false;

                break;
            case 1:
                fragment = new SellerDashboardFragment();
                fragment.setArguments(bundle);
                tvDashboard.setText(getString(R.string.seller_dashboard));
               /* imgBurtoggle.setVisibility(View.VISIBLE);
                imgSelltoggle.setVisibility(View.GONE);*/
                tvChngdash.setText("Visit Buyer Dashboard >");
                sel = true;

                break;
            case 2:
                startActivity(new Intent(Dashboard.this, ShopMallActivity.class));
                break;
            case 3:
                startActivity(new Intent(Dashboard.this, MyInquiry.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;

            case 4:
                drawerLayout.closeDrawer(Gravity.LEFT);
                //ProfileDialog(Dashboard.this);
                Intent myIntent = new Intent(Dashboard.this, ProfileChangeActivity.class);
                startActivity(myIntent);
                break;
            case 5:
                startActivity(new Intent(Dashboard.this, SettingActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case 6:
                startActivity(new Intent(Dashboard.this, ContactUsActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case 7:
                startActivity(new Intent(Dashboard.this, AboutUs.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case 8:
                startActivity(new Intent(Dashboard.this, TermsCondition.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case 9:
                alert();
                break;
            default:
                break;
        }

        if (fragment != null) {
            drawerLayout.closeDrawer(Gravity.LEFT);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.framelayout, fragment).commit();
        } else {
            Log.e("Dash", "Error in creating fragment");

        }
    }

    public void alert() {
        new AlertDialog.Builder(this)
                .setMessage((R.string.logout))
                .setPositiveButton("YES", (dialog, which) -> {
                    getLogout();
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void exitalert() {
        new AlertDialog.Builder(this)
                .setMessage(("Are you sure to Exit?"))
                .setPositiveButton("YES", (dialog, which) -> {
                    if (advertisementModels.size() > 0) {
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        liExitads.setVisibility(View.VISIBLE);
                        L.loadImageWithPicasso(this, advertisementModels.get(0).getAdvtImageURL(), imgAds, null);
                        firstclick = true;
                    /*    new Handler().postDelayed(() -> {
                            super.onBackPressed();
                            // finish();
                        }, IMAGE_TIME_OUT);*/
                    } else {
                        liExitads.setVisibility(View.GONE);
                        firstclick = false;
                        // super.onBackPressed();
                        finish();
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    private void getExitads() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getExitads(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data)) {
                        if (jsonResponse.getJSONObject(Constant.data).has(Constant.LogoutAdvt)) {
                            advertisementModels.clear();
                            advertisementModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.LogoutAdvt).toString(), AdvertisementModel.class));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void ProfileDialog(Activity activity) {
        profiledialog = new Dialog(activity, R.style.Theme_Dialog);
        profiledialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profiledialog.setCancelable(false);
        profiledialog.setContentView(R.layout.userprofile);
        EditText edtName, edtSurname, edtMobileno;
        Button btnupdate;
        ImageView imgclose;
        RadioGroup radioGroup;
        final RadioButton[] radioButton = {null};
        final String[] usertype = {"Buyer Dashboard"};

        edtName = profiledialog.findViewById(R.id.edt_Pname);
        edtSurname = profiledialog.findViewById(R.id.edt_Psurname);
        edtMobileno = profiledialog.findViewById(R.id.edt_Pmobileno);
        btnupdate = profiledialog.findViewById(R.id.btn_profileupdate);
        imgclose = profiledialog.findViewById(R.id.img_close);
        img_edtprofile = profiledialog.findViewById(R.id.img_EDTProfile);
        radioGroup = profiledialog.findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
                    radioButton[0] = profiledialog.findViewById(checkedId);
                    usertype[0] = radioButton[0].getText().toString();
                }
        );

        if (user.getProfile_pic() != null) {
            L.loadImageWithPicasso(Dashboard.this, user.getProfile_pic(), img_edtprofile, null);
        }
        edtName.setText(user.getUsername());
        edtSurname.setText(user.getSurname());
        edtMobileno.setText(user.getMobile());
        edtMobileno.setEnabled(false);

        imgclose.setOnClickListener(v -> profiledialog.dismiss());
        img_edtprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dashboard.this.updateProfile(L.getEditText(edtName), L.getEditText(edtSurname), usertype[0]);
                profiledialog.dismiss();
            }
        });
        profiledialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.img_drawer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_drawer:
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
        }
    }

    private void updateProfile(String username, String surname, String type) {
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Constant.USERName, RestApi.createRequestBody(username));
        map.put(Constant.SURNAME, RestApi.createRequestBody(surname));
        map.put(Constant.DASHBOARD_PREF, RestApi.createRequestBody(type));
        MultipartBody.Part body = null;

        if (imgFile != null && !TextUtils.isEmpty(imgFile.getAbsolutePath())) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile);
            body = MultipartBody.Part.createFormData(Constant.ProfileImage, imgFile.getName(), requestFile);
        }

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.updateProfile(map, body), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    JSONObject DATA = jsonResponse.getJSONObject(Constant.data);
                    UserData user = LoganSquare.parse(DATA.getJSONObject("user").toString(), UserData.class);

                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    Intent intent = new Intent(this, Dashboard.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getLogout() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DeviceID, L.getDeviceId(Dashboard.this));

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogout(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    L.logout(Dashboard.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    //get image
    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            boolean result = Utility.checkPermission(Dashboard.this);

            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                if (result)
                    cameraIntent();
                dialog.dismiss();
            } else if (items[item].equals("Choose from Library")) {
                userChoosenTask = "Choose from Library";
                if (result)
                    galleryIntent();
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                imgFile = createImageFile();
                if (imgFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            imgFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            } catch (Exception ex) {
                Toast.makeText(this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(imgFile.getAbsolutePath(), img_edtprofile);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imgFile = new File(Objects.requireNonNull(getFilePath(this, data.getData())));
        img_edtprofile.setImageBitmap(bm);
    }
}


