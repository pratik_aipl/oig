package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class ShopModel implements Serializable {

    @JsonField
    int ShopDetailID;
    @JsonField
    int UserID;
    @JsonField
    String ShopName;
    @JsonField
    String ShopPic;
    @JsonField
    String IsPaid;

    public String getIsPaid() {
        return IsPaid;
    }

    public void setIsPaid(String isPaid) {
        IsPaid = isPaid;
    }

    public int getShopDetailID() {
        return ShopDetailID;
    }

    public void setShopDetailID(int shopDetailID) {
        ShopDetailID = shopDetailID;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public String getShopPic() {
        return ShopPic;
    }

    public void setShopPic(String shopPic) {
        ShopPic = shopPic;
    }
}
