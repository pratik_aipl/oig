package com.oig.test.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.oig.test.R;
import com.oig.test.activity.activity.SubCategorysList;
import com.oig.test.activity.model.CategoryModel;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Dashboardadpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<CategoryModel> dashmodels;

    public Dashboardadpater(Context context, List<CategoryModel> dashmodels) {
        this.context = context;
        this.dashmodels = dashmodels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.dash_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        final CategoryModel dashmodel = dashmodels.get(position);

        holder.tv_catname.setText(dashmodel.getCategoryName());
        L.loadImageWithPicasso(context, dashmodel.getCategoryIconURL(), holder.img_catimg, null);

        holder.itemView.setOnClickListener(v ->
        {
            if (dashmodel.getSubCategories().size() != 0) {

                context.startActivity(new Intent(context, SubCategorysList.class)
                        .putExtra(Constant.category, dashmodel));
            } else {
                Toast.makeText(context, "No SubCategory", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public int getItemCount() {
        return dashmodels.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.img_catimg)
        ImageView img_catimg;
        @BindView(R.id.tv_catname)
        TextView tv_catname;


        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}