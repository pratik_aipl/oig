package com.oig.test.activity.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.activity.UploadProductsActivity;
import com.oig.test.activity.listner.AddRemoveProductImage;
import com.oig.test.activity.model.ImageData;
import com.oig.test.activity.util.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<ImageData> imageList;
    AddRemoveProductImage addRemoveProductImage;
    private static final String TAG = "ProductImageAdapter";
    public ProductImageAdapter(UploadProductsActivity context, List<ImageData> imageslists) {
        this.context = context;
        this.imageList = imageslists;
        addRemoveProductImage = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.images_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        Log.d(TAG, "onBindViewHolder: "+imageList.get(position).getImagePath());
        if (!TextUtils.isEmpty(imageList.get(position).getImagePath())) {
            holder.mSelectedImage.setVisibility(View.VISIBLE);
            holder.mImagePicker.setVisibility(View.GONE);
            holder.mImgDelete.setVisibility(View.VISIBLE);
            L.onCaptureImageResult(imageList.get(position).getImagePath(),holder.mSelectedImage);
            holder.mImgDelete.setOnClickListener(v -> addRemoveProductImage.onRemoveImage(position));
        }else {

            if (position == imageList.size() - 1) {
                holder.mSelectedImage.setVisibility(View.GONE);
                holder.mImagePicker.setVisibility(View.VISIBLE);
                holder.tvImgttl.setText("Add More");
                holder.mImageUpload.setImageResource(R.drawable.add);
                holder.mImgDelete.setVisibility(View.GONE);
            }
        }


        holder.mImagePicker.setOnClickListener((View v) -> {
            addRemoveProductImage.onAddImage(position);
        });

    }


    @Override
    public int getItemCount() {
        return imageList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.mSelectedImage)
        ImageView mSelectedImage;
        @BindView(R.id.mImgDelete)
        ImageView mImgDelete;
        @BindView(R.id.mImageUpload)
        ImageView mImageUpload;
        @BindView(R.id.tv_imgttl)
        TextView tvImgttl;
        @BindView(R.id.mImagePicker)
        LinearLayout mImagePicker;
        @BindView(R.id.rel_main)
        RelativeLayout relMain;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}