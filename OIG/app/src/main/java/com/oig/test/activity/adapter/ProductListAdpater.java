package com.oig.test.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.listner.ProductDetail;
import com.oig.test.activity.model.ProductsList;
import com.oig.test.activity.util.L;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProductListAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

  Context context;
  List<ProductsList> productsLists;
  List<ProductsList> productsListsCopy;
  ProductDetail prodtl;

  public ProductListAdpater(Context context, ProductDetail prodtls, List<ProductsList> productDetails) {
    this.context = context;
    prodtl = prodtls;
    this.productsLists = productDetails;
    this.productsListsCopy = productDetails;
  }


  @NonNull
  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

    return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_row, parent, false));
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

    ViewHolder holder = (ViewHolder) holderIn;
    ProductsList productsList = productsListsCopy.get(position);


    holder.adClose.setVisibility(View.GONE);

    if (productsList.getViewType().equalsIgnoreCase("ad")) {
      holder.adLayout.setVisibility(View.VISIBLE);
      L.loadImageWithPicasso(context, productsList.getAdvertisementModel().getAdvtImageURL(), holder.adImage, null);
    } else {
      holder.adLayout.setVisibility(View.GONE);
    }

    holder.tvProname.setText(L.capSentences(productsList.getProductName()));
    holder.tvprolocation.setText(productsList.getLocationName());
    holder.tvProsku.setText(productsList.getSkuCode());
    holder.tv_more.setText(Html.fromHtml("<u>View More</u>"));
    // holder.tvProdesc.setText(L.capSentences(productsList.getProductDesc()));


    if (productsList.getProductImages() != null && productsList.getProductImages().size() > 0 && !TextUtils.isEmpty(productsList.getProductImages().get(0).getImageURL())) {
      L.loadImageWithPicasso(context, productsList.getProductImages().get(0).getImageURL(), holder.imgProduct, null);
    } else {
      holder.imgProduct.setImageResource(R.drawable.image_not_available);
    }


    holder.adImage.setOnClickListener(v -> {
      String url = productsList.getAdvertisementModel().getAdvtwebURL().contains("http") ? productsList.getAdvertisementModel().getAdvtwebURL() : "http://" + productsList.getAdvertisementModel().getAdvtwebURL();
      context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    });
    holder.liProducts.setOnClickListener(v -> {
      prodtl.proddetail(productsList);
    });
  }

  @Override
  public int getItemCount() {

    return productsListsCopy.size();
  }

  @Override
  public Filter getFilter() {
    return new Filter() {
      @Override
      protected FilterResults performFiltering(CharSequence charSequence) {
        String charString = charSequence.toString();
        if (charString.isEmpty()) {
          productsListsCopy = productsLists;
        } else {
          List<ProductsList> filteredList = new ArrayList<>();
          for (ProductsList row : productsLists) {
            if (row.getProductName().toLowerCase().contains(charString.toLowerCase())) {
              filteredList.add(row);
            }
          }

          productsListsCopy = filteredList;
        }

        FilterResults filterResults = new FilterResults();
        filterResults.values = productsListsCopy;
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        productsListsCopy = (ArrayList<ProductsList>) filterResults.values;
        notifyDataSetChanged();
      }
    };
  }


  static class ViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.img_Product)
    ImageView imgProduct;
    @BindView(R.id.tv_proname)
    TextView tvProname;
    @BindView(R.id.tv_prosku)
    TextView tvProsku;
    @BindView(R.id.tv_prolocation)
    TextView tvprolocation;
    /*@BindView(R.id.tv_prodesc)
    TextView tvProdesc;*/
    @BindView(R.id.tv_more)
    TextView tv_more;

    @BindView(R.id.li_prod)
    LinearLayout liProducts;
    @BindView(R.id.rel_ads)
    RelativeLayout adLayout;
    @BindView(R.id.img_ads)
    ImageView adImage;
    @BindView(R.id.img_closeads)
    ImageView adClose;


    ViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

}