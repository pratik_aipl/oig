package com.oig.test.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.oig.test.R;
import com.oig.test.activity.model.AdvertisementModel;
import com.oig.test.activity.util.L;

import java.util.List;

public class AdPagerAdpater extends PagerAdapter {


    List<AdvertisementModel> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public AdPagerAdpater(Context context, List<AdvertisementModel> IMAGES) {
        this.context = context;
        this.IMAGES = IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.imageview, view, false);

        assert imageLayout != null;
        AdvertisementModel advertisementModel = IMAGES.get(position);
        final ImageView imageView = imageLayout.findViewById(R.id.img_ads);

        L.loadImageWithPicasso(context, advertisementModel.getAdvtImageURL(), imageView, null);

        imageView.setOnClickListener(v -> {
            if (IMAGES != null && IMAGES.size() > 0) {
                String url = advertisementModel.getAdvtwebURL().contains("http") ? advertisementModel.getAdvtwebURL() : "http://" + advertisementModel.getAdvtwebURL();
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            }
        });

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}