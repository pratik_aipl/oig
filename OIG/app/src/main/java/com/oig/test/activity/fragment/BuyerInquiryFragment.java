package com.oig.test.activity.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.R;
import com.oig.test.activity.adapter.SellerDashboardadpater;
import com.oig.test.activity.model.BuyersInquiryModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

public class BuyerInquiryFragment extends BaseFragment {

    List<BuyersInquiryModel> buyersInquiryModels = new ArrayList<>();

    SellerDashboardadpater sellerDashboardadpater;
    Subscription subscription;
    LinearLayout notfound;
    RecyclerView recyclerList1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layoutView = inflater.inflate(R.layout.buyerinquiryfrag, container, false);

        recyclerList1 = layoutView.findViewById(R.id.recycler_list);
        FloatingActionButton fabAdd = layoutView.findViewById(R.id.fab_add);
        notfound = layoutView.findViewById(R.id.li_notfound);
        fabAdd.setVisibility(View.GONE);

        if (L.isNetworkAvailable(getActivity()))
            getBuyerInq();

        recyclerList1.setHasFixedSize(true);
        recyclerList1.setItemAnimator(new DefaultItemAnimator());
        recyclerList1.setLayoutManager(new LinearLayoutManager(getActivity()));

        sellerDashboardadpater = new SellerDashboardadpater(getActivity(), buyersInquiryModels, user.getIsPaid());
        recyclerList1.setAdapter(sellerDashboardadpater);

        return layoutView;
    }

    private void getBuyerInq() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getBuyerInquiry(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if (jsonResponse.has(Constant.data))
                        if (jsonResponse.getJSONObject(Constant.data).has(Constant.MyInquiry)) {
                            buyersInquiryModels.clear();
                            buyersInquiryModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.MyInquiry).toString(), BuyersInquiryModel.class));
                            sellerDashboardadpater.notifyDataSetChanged();
                        } else {
                            recyclerList1.setVisibility(View.GONE);
                            notfound.setVisibility(View.VISIBLE);
                        }

                } catch (Exception e) {
                    recyclerList1.setVisibility(View.GONE);
                    notfound.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

}
