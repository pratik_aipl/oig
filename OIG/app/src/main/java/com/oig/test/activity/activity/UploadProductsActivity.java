package com.oig.test.activity.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.oig.test.BuildConfig;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.adapter.ProductImageAdapter;
import com.oig.test.activity.listner.AddRemoveProductImage;
import com.oig.test.activity.model.CategoryModel;
import com.oig.test.activity.model.FiledModel;
import com.oig.test.activity.model.ImageData;
import com.oig.test.activity.model.LocationModel;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.network.RestApi;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;
import com.oig.test.activity.util.Utility;
import com.oig.test.activity.util.Validation;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscription;

import static com.oig.test.activity.util.ImagePathUtils.getFilePath;
import static com.oig.test.activity.util.L.onCaptureImageResult;

public class UploadProductsActivity extends BaseActivity implements AddRemoveProductImage, AdapterView.OnItemSelectedListener {

    private static final String TAG = "UploadProductsActivity";

    private static final int REQUEST_CAMERA = 0;
    private static final int SELECT_FILE = 1;

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.spn_category)
    Spinner spnCategory;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.li_mainview)
    LinearLayout liMainview;
    @BindView(R.id.edt_prodname)
    EditText edtProdname;
    @BindView(R.id.edt_skucode)
    EditText edtSkucode;
    @BindView(R.id.edt_desc)
    EditText edtDesc;
    @BindView(R.id.btn_create)
    Button btnCreate;
    @BindView(R.id.mCameraImage)
    ImageView mCameraImage;
    @BindView(R.id.spn_subcategory)
    Spinner spnSubcategory;

    CategoryAdapter categoryAdapter;
    SubCategoryAdapter subCategoryAdapter;
    Subscription subscription;
    List<CategoryModel> categoryModels = new ArrayList<>();
    List<CategoryModel> subcategoryList = new ArrayList<>();
    List<LocationModel> location = new ArrayList<>();

    List<FiledModel> filedModelsList = new ArrayList<>();
    int subcat;
    String locationId;
    UploadLocationAdapter UploadLocationAdapter;

    final List<EditText> allEds = new ArrayList<>();

    @BindView(R.id.li_requird)
    LinearLayout liRequird;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerlist;

    List<ImageData> imageList = new ArrayList<>();
    File image;
    ProductImageAdapter productImageAdapter;
    String userChoosenTask;

    int selectPos = -1;

    @BindView(R.id.li_imagelist)
    LinearLayout liImagelist;
    @BindView(R.id.spn_location)
    Spinner spnLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_products);
        ButterKnife.bind(this);

        tvTittle.setText("Upload Product");
        if (L.isNetworkAvailable(this))
            getcategory();
        if (L.isNetworkAvailable(this))
            getLocation();

        spnLocation.setOnItemSelectedListener(this);

        UploadLocationAdapter = new UploadLocationAdapter(this, location);
        spnLocation.setAdapter(UploadLocationAdapter);

        categoryAdapter = new CategoryAdapter(this, categoryModels);
        spnCategory.setAdapter(categoryAdapter);

        subCategoryAdapter = new SubCategoryAdapter(getApplicationContext(), subcategoryList);
        spnSubcategory.setAdapter(subCategoryAdapter);

        for (int i = 0; i < 1; i++) {
            imageList.add(new ImageData("", null));
        }

        recyclerlist.setHasFixedSize(true);
        recyclerlist.setItemAnimator(new DefaultItemAnimator());
        recyclerlist.setLayoutManager(new GridLayoutManager(this, 3));

        productImageAdapter = new ProductImageAdapter(this, imageList);
        recyclerlist.setAdapter(productImageAdapter);

        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemSelected cat: " + position);
                spnSubcategory.setSelection(0);
                liMainview.removeAllViews();
                liRequird.setVisibility(View.GONE);
                liImagelist.setVisibility(View.GONE);

                changeButton(true);
                if (position != 0) {
                    subcategoryList.clear();
                    subcategoryList.addAll(categoryModels.get(position - 1).getSubCategories());
                    subCategoryAdapter.notifyDataSetChanged();
                } else {
                    subcategoryList.clear();
                    subCategoryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                subcategoryList.clear();
                subCategoryAdapter.notifyDataSetChanged();
            }
        });
        spnSubcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                liMainview.removeAllViews();
                liRequird.setVisibility(View.GONE);
                liImagelist.setVisibility(View.GONE);
                Log.d(TAG, "onItemSelected: " + position);
                changeButton(true);
                if (position != 0) {
                    subcat = subcategoryList.get(position - 1).getCategoryID();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


    }

    @OnClick({R.id.img_back, R.id.btn_submit, R.id.btn_create})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.btn_create:
                if (categoryModels.size() == 0 || spnCategory.getSelectedItemPosition() == 0) {
                    Toast.makeText(this, "Please Select Category", Toast.LENGTH_SHORT).show();
                } else if (subcategoryList.size() == 0 || spnSubcategory.getSelectedItemPosition() == 0) {
                    Toast.makeText(this, "Please Select Sub Category", Toast.LENGTH_SHORT).show();
                } else if (location.size() == 0 || spnLocation.getSelectedItemPosition() == 0) {
                    Toast.makeText(this, "Please Select Location", Toast.LENGTH_SHORT).show();
                } else {
                    if (L.isNetworkAvailable(UploadProductsActivity.this)) {
                        getFiledList();
                    }
                }
                break;
            case R.id.btn_submit:
                if (Validation.isEmpty(L.getEditText(edtProdname))) {
                    edtProdname.setError("Please enter ProductName");
                } else if (Validation.isEmpty(L.getEditText(edtDesc))) {
                    edtDesc.setError("Please enter Description");
                } else if (location.size() == 0 || spnLocation.getSelectedItemPosition() == 0) {
                    Toast.makeText(this, "Please Select Location", Toast.LENGTH_SHORT).show();
                } else {
                    if (L.isNetworkAvailable(UploadProductsActivity.this)) {
                        uploadProducts();
                    }
                }
                break;
        }
    }

    public void getLocation() {
        Map<String, String> map = new HashMap<>();
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLocation(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    location.clear();
                    location.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray("City").toString(), LocationModel.class));
                    UploadLocationAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }
        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void getcategory() {
        Map<String, String> map = new HashMap<>();

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getcategory(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    categoryModels.clear();
                    categoryModels.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.Categories).toString(), CategoryModel.class));
                    categoryAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    private void getFiledList() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getFiledList(subcat), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    filedModelsList.clear();
                    filedModelsList.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.Fields).toString(), FiledModel.class));

                    allEds.clear();
                    if (filedModelsList.size() != 0) {

                        addView();
                        changeButton(false);
                        liRequird.setVisibility(View.VISIBLE);
                        liImagelist.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void changeButton(boolean isCreate) {

        if (!isCreate) {
            btnSubmit.setVisibility(View.VISIBLE);
            btnCreate.setVisibility(View.GONE);
        } else {
            btnSubmit.setVisibility(View.GONE);
            btnCreate.setVisibility(View.VISIBLE);
        }
    }

    public void uploadProducts() {
        Map<String, RequestBody> map = new HashMap<>();
        map.put(Constant.CategoryID, RestApi.createRequestBody(String.valueOf(subcat)));
        map.put(Constant.ProductType, RestApi.createRequestBody("OLD"));
        map.put(Constant.ProductName, RestApi.createRequestBody(L.getEditText(edtProdname)));
        map.put(Constant.ProductDesc, RestApi.createRequestBody(L.getEditText(edtDesc)));
        map.put(Constant.LOCATION_ID, RestApi.createRequestBody(locationId));
        map.put(Constant.skuCode, RestApi.createRequestBody(L.getEditText(edtSkucode)));

        for (int i = 0; i < allEds.size(); i++) {
            map.put(filedModelsList.get(i).getFields(), RestApi.createRequestBody(L.getEditText(allEds.get(i))));
        }

        List<MultipartBody.Part> parts = new ArrayList<>();
        for (int i = 0; i < imageList.size(); i++) {
            if (imageList.get(i).getFile() != null && !TextUtils.isEmpty(imageList.get(i).getImagePath())) {
                RequestBody requestFile = null;
                try {
                    requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), new Compressor(this).compressToFile(imageList.get(i).getFile()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                parts.add(MultipartBody.Part.createFormData("ProductImage[]", imageList.get(i).getFile().getName(), requestFile));
            }
        }
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.uploadProduct(map, parts), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Toast.makeText(this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(UploadProductsActivity.this, Dashboard.class).putExtra(Constant.selectTab, 1).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    public void addView() {
        // add edittext
        for (int i = 0; i < filedModelsList.size(); i++) {
            TextView textView = new TextView(this);
            EditText editText = new EditText(this);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LinearLayout.LayoutParams tp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            final float scale = getResources().getDisplayMetrics().density;
            tp.height = (int) (20 * scale);
            p.height = (int) (50 * scale);
            allEds.add(editText);
            tp.setMargins((int) (11 * scale), (int) (7 * scale), 0, 0);
            p.setMargins(0, (int) (11 * scale), 0, 0);
            textView.setLayoutParams(tp);
            editText.setLayoutParams(p);
            textView.setText(filedModelsList.get(i).getLabel());
            textView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            editText.setHint(filedModelsList.get(i).getLabel());
            editText.setHintTextColor(Color.GRAY);
            editText.setBackground(ContextCompat.getDrawable(this, R.drawable.edittextbox));
            editText.setTextColor(Color.BLACK);
            editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            editText.setMaxLines(1);
            editText.setSingleLine();
            editText.setPadding((int) (10 * scale), 0, 0, 0);
            liMainview.addView(textView);
            liMainview.addView(editText);
        }
    }

    @Override
    public void onAddImage(int pos) {
        selectPos = pos;
        if (imageList.size() <= 4) {
            selectImage();
        } else {
            Toast.makeText(this, "Add Maximum 4 images", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRemoveImage(int position) {
        imageList.remove(position);
        productImageAdapter.notifyDataSetChanged();
    }

    public void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, (dialog, item) -> {
            boolean result = Utility.checkPermission(UploadProductsActivity.this);

            if (items[item].equals("Take Photo")) {
                userChoosenTask = "Take Photo";
                if (result)
                    cameraIntent();
            } else if (items[item].equals("Choose from Library")) {
                userChoosenTask = "Choose from Library";
                if (result)
                    galleryIntent();
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


    public void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                File imgFile = createImageFile();
                if (imgFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".fileprovider",
                            imgFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            } catch (Exception ex) {
                Toast.makeText(this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        return image;
    }

    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(image.getAbsolutePath(), mCameraImage);
                imageList.add(new ImageData(image.getAbsolutePath(), image));
                productImageAdapter.notifyDataSetChanged();
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        imageList.add(new ImageData(getFilePath(this, data.getData()), new File(getFilePath(this, data.getData()))));
        productImageAdapter.notifyDataSetChanged();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (position != 0)
            locationId = "" + location.get(position - 1).getID();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

class CategoryAdapter extends BaseAdapter {
    List<CategoryModel> categoryModels;
    LayoutInflater inflter;
    Context context;

    public CategoryAdapter(Context context, List<CategoryModel> categoryModels) {
        this.context = context;
        this.categoryModels = categoryModels;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return categoryModels.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Category");
            names.setTextColor(Color.GRAY);
        } else {
            CategoryModel categoryModel = categoryModels.get(position - 1);
            names.setText(categoryModel.getCategoryName());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class SubCategoryAdapter extends BaseAdapter {
    List<CategoryModel> subcategoryList;
    LayoutInflater inflter;
    Context context;

    public SubCategoryAdapter(Context context, List<CategoryModel> subcategoryList) {
        this.context = context;
        this.subcategoryList = subcategoryList;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return subcategoryList.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return subcategoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Sub Category");
            names.setTextColor(Color.GRAY);
        } else {
            CategoryModel categoryModel = subcategoryList.get(position - 1);
            names.setText(categoryModel.getCategoryName());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }


}

class UploadLocationAdapter extends BaseAdapter {
    List<LocationModel> locationModels;
    LayoutInflater inflter;
    Context context;

    public UploadLocationAdapter(Context context, List<LocationModel> locationModels) {
        this.context = context;
        this.locationModels = locationModels;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return locationModels.size() + 1;
    }

    @Override
    public Object getItem(int position) {
        return locationModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.custom_spinner_items, null);
        TextView names = convertView.findViewById(R.id.tv_spinner);
        if (position == 0) {
            names.setText("Location");
            names.setTextColor(Color.GRAY);
        } else {
            LocationModel locationModel = locationModels.get(position - 1);
            names.setText(locationModel.getValue());
            names.setTextColor(Color.BLACK);
        }
        return convertView;
    }

}

