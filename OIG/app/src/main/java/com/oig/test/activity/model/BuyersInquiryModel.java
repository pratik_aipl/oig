package com.oig.test.activity.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class BuyersInquiryModel {

    @JsonField
    int InquiryID;
    @JsonField
    String username;
    @JsonField
    String surname;
    @JsonField
    String InquiryNo;
    @JsonField
    String ProductName;
    @JsonField
    String skuCode;
    @JsonField
    String CreatedOn;
    @JsonField
    String mobile;
    @JsonField
    String admin_number;

    @JsonField
    List<ProductsImage> ProductImages;

    public List<ProductsImage> getProductImages() {
        return ProductImages;
    }

    public void setProductImages(List<ProductsImage> productImages) {
        ProductImages = productImages;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAdmin_number() {
        return admin_number;
    }

    public void setAdmin_number(String admin_number) {
        this.admin_number = admin_number;
    }

    public int getInquiryID() {
        return InquiryID;
    }

    public void setInquiryID(int inquiryID) {
        InquiryID = inquiryID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getInquiryNo() {
        return InquiryNo;
    }

    public void setInquiryNo(String inquiryNo) {
        InquiryNo = inquiryNo;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }
}
