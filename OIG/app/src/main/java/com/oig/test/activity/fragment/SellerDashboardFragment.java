package com.oig.test.activity.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oig.test.R;
import com.oig.test.activity.adapter.TabPagerAdapter;
import com.oig.test.activity.util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class SellerDashboardFragment extends Fragment {

    @BindView(R.id.tab_Inquiry)
    TabLayout tabInquiry;
    @BindView(R.id.pager_Inq)
    ViewPager pagerInq;
    private Unbinder unbinder;

    boolean myInt = false;
    int tab = 0;

    public SellerDashboardFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.seller_dashboard, container, false);
        unbinder = ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            myInt = bundle.getBoolean(Constant.TabIndex, false);
            tab = bundle.getInt(Constant.tab, -1);
        }

        setupViewPager(pagerInq);
        pagerInq.setOffscreenPageLimit(2);
        tabInquiry.setupWithViewPager(pagerInq);

        if (tab != -1)
            tabInquiry.getTabAt(tab).select();
        else {
            if (myInt) {
                tabInquiry.getTabAt(1).select();
                myInt = false;
            } else {
                tabInquiry.getTabAt(0).select();
            }
        }
        return view;
    }


    private void setupViewPager(ViewPager viewPager) {
        TabPagerAdapter viewPagerAdapter = new TabPagerAdapter(getFragmentManager());

        viewPagerAdapter.addFragment(new MyProductFragment(), "PRODUCT TO SELL");
        viewPagerAdapter.addFragment(new BuyerInquiryFragment(), "BUYER'S INQUIRY");

        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
