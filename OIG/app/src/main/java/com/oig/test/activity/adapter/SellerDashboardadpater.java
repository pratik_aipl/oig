package com.oig.test.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.oig.test.R;
import com.oig.test.activity.model.BuyersInquiryModel;
import com.oig.test.activity.util.L;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SellerDashboardadpater extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<BuyersInquiryModel> buyersInquiryModel;

    String ispaid;

    public SellerDashboardadpater(Context context, List<BuyersInquiryModel> buyersInquiryModels, String isPaid) {
        this.context = context;
        this.buyersInquiryModel = buyersInquiryModels;
        this.ispaid = isPaid;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.seller_list_row, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holderIn, final int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        BuyersInquiryModel buyersInquiryModellist = buyersInquiryModel.get(position);

        holder.tvProname.setText(buyersInquiryModellist.getProductName());
        holder.tvInqno.setText(buyersInquiryModellist.getInquiryNo());
        holder.tvSku.setText(buyersInquiryModellist.getSkuCode());
        holder.tvUsername.setText(buyersInquiryModellist.getUsername() + " " + buyersInquiryModellist.getSurname());
        holder.tvDate.setText(L.getDate(buyersInquiryModellist.getCreatedOn()));
        if (buyersInquiryModellist.getProductImages().size() != 0)
            L.loadImageWithPicasso(context, buyersInquiryModellist.getProductImages().get(0).getImageURL(), holder.imgProduct, null);

        String phone;

        if (ispaid.equalsIgnoreCase("0")) {
            phone = buyersInquiryModellist.getAdmin_number();
        } else {
            phone = buyersInquiryModellist.getMobile();
        }
        holder.imgContact.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return buyersInquiryModel.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_proname)
        TextView tvProname;
        @BindView(R.id.tv_inqno)
        TextView tvInqno;
        @BindView(R.id.tv_sku)
        TextView tvSku;
        @BindView(R.id.tv_username)
        TextView tvUsername;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.img_contact)
        ImageView imgContact;
        @BindView(R.id.img_Product)
        ImageView imgProduct;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}