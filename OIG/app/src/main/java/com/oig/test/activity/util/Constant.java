package com.oig.test.activity.util;

public class Constant {

    public static final String IsFrom = "isfrom";
    public static final String Img_Path = "Img_path";
    public static final String Categories = "Categories";
    public static final String ShopCategories = "ShopCategories";
    public static final String Products = "Products";
    public static final String CategoryID = "CategoryID";
    public static final String ProductsImages = "ProductImages";
    public static final String MyInquiry = "Inquiry";
    public static final String Shops = "Shops";
    public static final String CatId = "CatId";
    public static final String CatType = "CatType";
    public static final String SubID = "SubId";
    public static final String Fields = "Fields";
    public static final String ProductName = "ProductName";
    public static final String ProductDesc = "ProductDesc";
    public static final String ProductType = "ProductType";
    public static final String ProID = "ProID";
    public static final String ProductID = "ProductID";
    public static final String IsPaid = "IsPaid";
    public static final String Advt = "Advt";
    public static final String LogoutAdvt = "LogoutAdvt";
    public static final String NotificationStatus = "NotificationStatus";
    public static final String PlayerID = "PlayerID";
    public static final String EMAIL = "EmailID";
    public static final String ChildTab = "ChildTab";
    public static final String TabIndex ="TabIndex" ;
    public static String UserData = "UserData";
    public static String image = "image";
    public static String video = "video";
    public static String audio = "audio";
    public static String content = "content";
    public static String file = "file";

    public static final String DeviceID = "DeviceID";
    public static final String ProfileImage = "profile_pic";
    public static boolean isAlertShow = false;
    public static String message = "message";
    public static String isLogin = "isLogin";
    public static String Otp = "OTP";
    public static String loginAuthToken = "loginAuthToken";
    public static String USERID = "id";
    public static String data = "data";


    public static String USERName = "username";
    public static String SURNAME = "surname";
    public static String MOBILE = "mobile";
    public static String MOBILENO = "MobileNo";
    public static String GENDER = "gender";
    public static String DOB = "dob";
    public static String LOCATION_ID = "location_id";
    public static String DASHBOARD_PREF = "dashboard_pref";

    public static String category = "category";
    public static String skuCode = "skuCode";
    public static String selectTab = "selectTab";
    public static String admin="admin";
    public static String selectindex="selectindex";
    public static String actindex="actindex";
    public static String tab="tab";
}
