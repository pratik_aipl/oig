package com.oig.test.activity.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.oig.test.R;
import com.oig.test.activity.BaseActivity;
import com.oig.test.activity.network.NetworkRequest;
import com.oig.test.activity.util.Constant;
import com.oig.test.activity.util.L;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class SettingActivity extends BaseActivity {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;

    Subscription subscription;

    @BindView(R.id.switch_noti)
    Switch switchNoti;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        tvTittle.setText("Settings");

        if (user.getNotificationStatus().equalsIgnoreCase("1")) {
            switchNoti.setChecked(true);
        } else {
            switchNoti.setChecked(false);
        }

      /*1 IS ON
0 IS OFF*/
        switchNoti.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                notificationStatus("1");
            } else {
                notificationStatus("0");
            }

        });
    }

    @OnClick({R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                onBackPressed();
                break;
        }
    }

    public void alert() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.deactiveacc)
                .setPositiveButton("DEACTIVATE", (dialog, which) -> {
                    removeAccount();
                })
                .setNegativeButton("CANCEL", null)
                .show();
    }

    private void removeAccount() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.removeAccount(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());

                    L.logout(SettingActivity.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            // showConnectionSnackBarUserLogin();
        });

    }


    private void notificationStatus(String status) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.NotificationStatus, status);

        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.notificationStatus(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    user.setNotificationStatus(status);
                    prefs.save(Constant.UserData, new Gson().toJson(user));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
